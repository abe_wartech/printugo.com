<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Customer;
use App\Product;

class ApiController extends Controller
{
    public function blog()
    {
        $data = Blog::all();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function customer()
    {
        $data = Customer::all();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function product()
    {
        $data = Product::all();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function homeproduct()
    {
        $data = Product::where('is_home', 1)->limit(6)->get();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function search()
    {
        $data = Product::where('title', 'LIKE', '%' . request('q') . '%')->get();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function oneproduct()
    {
        $data = Product::where('id', request('id'))->get();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function oneblog()
    {
        $data = Blog::where('id', request('id'))->get();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }
}
