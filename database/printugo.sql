-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Apr 2020 pada 12.59
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `printugo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `resume` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `desc`, `image`, `created_at`, `updated_at`, `resume`) VALUES
(1, 'Grab Transport Advertising', 'Malesuada facilisi velit tellus imperdiet consectetur velit cursus enim eu. Hac eleifend eget tortor massa vitae ut blandit proin feugiat. Sollicitudin vel feugiat nisl tincidunt. Lectus feugiat enim laoreet fusce maecenas amet, odio sociis. Enim, elementum, est nunc ut velit. Consequat, augue fermentum hendrerit molestie mi. Odio lorem in donec felis enim, morbi donec. Aenean sapien felis, eget vulputate maecenas etiam. Turpis sit odio ullamcorper habitasse auctor morbi semper arcu. Vitae faucibus interdum aliquam nulla. Vestibulum malesuada eros, sit diam morbi ornare amet, sodales blandit. Netus hac ipsum ac non turpis commodo in hendrerit. Consequat ut tellus amet integer pharetra nec sed at sit. Nibh praesent eu nisl in ipsum, sit tellus vitae sit. Vitae lacus, egestas scelerisque nam justo, posuere. Tristique nulla leo nisi, placerat mauris, in nisl justo ornare. Nunc lorem purus, volutpat ridiculus sem sed sed. Vulputate et orci rutrum dignissim vel vel vel. Nulla donec amet molestie nam placerat. Cursus egestas quis tincidunt convallis ultricies eu. Ultrices id dignissim nunc vel felis accumsan turpis nam. Neque lacus, etiam etiam condimentum aliquam et aliquet sit quam. Faucibus vulputate volutpat viverra at faucibus vitae. Placerat orci lectus suscipit nibh in semper diam diam eget. At rhoncus a tortor libero enim. Scelerisque nulla at dictum ornare lorem urna, enim. Sed mattis varius eu fringilla morbi nibh urna commodo. Amet gravida scelerisque leo, cursus etiam facilisis. Non sed lacus aliquet pharetra in egestas vel posuere. Euismod mi praesent nibh tellus scelerisque massa duis. Nunc, ornare nullam leo facilisis turpis porttitor ut volutpat etiam. Non mus feugiat aliquet nisi non suspendisse. Ut mollis malesuada lectus elementum nec, maecenas elementum. Odio porttitor cursus eget cursus congue turpis. Cursus feugiat purus rhoncus et molestie tortor non id metus. Aliquam, platea tellus eu risus, et quis quam scelerisque velit. Blandit elit fringilla facilisis massa eu. Mauris vulputate hendrerit in id non non egestas vel bibendum. Viverra neque, euismod et pellentesque in. Sit laoreet mauris euismod placerat cursus dolor, lorem. Etiam nulla vulputate nec est rutrum lectus vitae nibh augue. Sem commodo mauris in orci varius. Pharetra, nunc turpis id sed nullam egestas. Neque, magnis dolor odio mauris quam dictumst habitasse ultrices.', 'blogs\\March2020\\Wf1dT58P1A84aZRht5G1.png', '2020-03-24 23:39:00', '2020-04-04 00:53:11', 'Kami menyediakan jasa percetakan untuk  kebutuhan pemasaran brand anda');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kategori` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `customers`
--

INSERT INTO `customers` (`id`, `title`, `image`, `harga`, `created_at`, `updated_at`, `kategori`, `desc`) VALUES
(1, 'Photo Print', 'customers\\March2020\\rLXGZ3zF96luGuCikGKI.png', 'Rp.664,000', '2020-03-25 23:44:00', '2020-04-05 03:36:41', 'transport', '<p><span style=\"color: rgba(0, 0, 0, 0.65); font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; background-color: #f5f5f5;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius quis in sagittis aliquet pellentesque eget ultrices tempor sed. Metus massa est quis volutpat, netus praesent tellus aliquet. Molestie enim mauris est etiam at risus. Massa proin at sodales dolor lectus nisl vel sit dictum. Fermentum sed enim nunc dui diam non. Neque, eu arcu auctor donec aenean mauris. Morbi sed tincidunt arcu, volutpat rhoncus blandit. Sed consequat enim eget gravida aliquam risus, sem. Montes, ante nisi quis urna sed neque, egestas hac. Aliquet ornare sagittis senectus amet, tellus sed est. Facilisi porta elit, condimentum porttitor lorem. Ut. &bull; Maecenas amet tempor est &bull; Maecenas amet tempor est &bull; Maecenas amet tempor est Maecenas amet tempor est, aliquet netus gravida enim. Tortor cras enim egestas vulputate vitae et, elementum volutpat magna. Aliquam in arcu dictum sodales porta morbi suspendisse odio. Consequat egestas tincidunt vulputate hac aliquet nam purus euismod. Sit viverra convallis nulla eleifend netus viverra phasellus. Placerat consequat lorem nibh elit est. Dui, ornare nibh adipiscing duis nulla ullamcorper orci id.</span></p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 5, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(24, 5, 'desc', 'text_area', 'Desc', 0, 1, 1, 1, 1, 1, '{}', 3),
(25, 5, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(26, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(27, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(28, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(29, 6, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(30, 6, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(31, 6, 'harga', 'text', 'Harga', 0, 1, 1, 1, 1, 1, '{}', 4),
(32, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(33, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(34, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(35, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(36, 7, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(37, 7, 'harga', 'text', 'Harga', 0, 1, 1, 1, 1, 1, '{}', 4),
(38, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(39, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(40, 7, 'kategori', 'select_dropdown', 'Kategori', 0, 1, 1, 1, 1, 1, '{\"default\":\"foto\",\"options\":{\"foto\":\"Foto\",\"promosi\":\"Kebutuhan Promosi\",\"kantor\":\"Kebutuhan Kantor\",\"kartu\":\"Kartu Nama\",\"pack\":\"Packaging\",\"merc\":\"Merchandise\"}}', 7),
(41, 7, 'desc', 'rich_text_box', 'Desc', 0, 1, 1, 1, 1, 1, '{}', 8),
(42, 5, 'resume', 'text_area', 'Resume', 0, 1, 1, 1, 1, 1, '{}', 7),
(43, 6, 'kategori', 'select_dropdown', 'Kategori', 0, 1, 1, 1, 1, 1, '{\"default\":\"transport\",\"options\":{\"transport\":\"Transport Advertising\",\"marketing\":\"Marketing Prints\",\"office\":\"Office Needs Prints\",\"signage\":\"Signage Advertising\",\"garmen\":\"Garmen Prints\",\"packaging\":\"Packaging Prints\"}}', 7),
(44, 6, 'desc', 'rich_text_box', 'Desc', 0, 1, 1, 1, 1, 1, '{}', 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(4, 'blog', 'blog', 'Blog', 'Blogs', 'voyager-news', 'App\\Blog', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-24 23:35:12', '2020-03-24 23:35:12'),
(5, 'blogs', 'blogs', 'Blog', 'Blogs', 'voyager-news', 'App\\Blog', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-24 23:38:20', '2020-04-04 00:52:36'),
(6, 'customers', 'customers', 'Customer', 'Customers', 'voyager-smile', 'App\\Customer', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-25 23:43:24', '2020-04-05 03:36:09'),
(7, 'products', 'products', 'Product', 'Products', 'voyager-shop', 'App\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-25 23:46:51', '2020-03-28 19:32:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-03-20 04:53:17', '2020-03-20 04:53:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-03-20 04:53:17', '2020-03-20 04:53:17', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-03-20 04:53:17', '2020-03-20 04:53:17', 'voyager.hooks', NULL),
(13, 1, 'Blogs', '', '_self', 'voyager-news', NULL, NULL, 16, '2020-03-24 23:38:20', '2020-03-24 23:38:20', 'voyager.blogs.index', NULL),
(14, 1, 'Customers', '', '_self', 'voyager-smile', NULL, NULL, 17, '2020-03-25 23:43:25', '2020-03-25 23:43:25', 'voyager.customers.index', NULL),
(15, 1, 'Products', '', '_self', 'voyager-shop', NULL, NULL, 18, '2020-03-25 23:46:51', '2020-03-25 23:46:51', 'voyager.products.index', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(2, 'browse_bread', NULL, '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(3, 'browse_database', NULL, '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(4, 'browse_media', NULL, '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(5, 'browse_compass', NULL, '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(6, 'browse_menus', 'menus', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(7, 'read_menus', 'menus', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(8, 'edit_menus', 'menus', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(9, 'add_menus', 'menus', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(10, 'delete_menus', 'menus', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(11, 'browse_roles', 'roles', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(12, 'read_roles', 'roles', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(13, 'edit_roles', 'roles', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(14, 'add_roles', 'roles', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(15, 'delete_roles', 'roles', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(16, 'browse_users', 'users', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(17, 'read_users', 'users', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(18, 'edit_users', 'users', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(19, 'add_users', 'users', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(20, 'delete_users', 'users', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(21, 'browse_settings', 'settings', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(22, 'read_settings', 'settings', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(23, 'edit_settings', 'settings', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(24, 'add_settings', 'settings', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(25, 'delete_settings', 'settings', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(26, 'browse_hooks', NULL, '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(27, 'browse_blog', 'blog', '2020-03-24 23:35:12', '2020-03-24 23:35:12'),
(28, 'read_blog', 'blog', '2020-03-24 23:35:12', '2020-03-24 23:35:12'),
(29, 'edit_blog', 'blog', '2020-03-24 23:35:12', '2020-03-24 23:35:12'),
(30, 'add_blog', 'blog', '2020-03-24 23:35:12', '2020-03-24 23:35:12'),
(31, 'delete_blog', 'blog', '2020-03-24 23:35:12', '2020-03-24 23:35:12'),
(32, 'browse_blogs', 'blogs', '2020-03-24 23:38:20', '2020-03-24 23:38:20'),
(33, 'read_blogs', 'blogs', '2020-03-24 23:38:20', '2020-03-24 23:38:20'),
(34, 'edit_blogs', 'blogs', '2020-03-24 23:38:20', '2020-03-24 23:38:20'),
(35, 'add_blogs', 'blogs', '2020-03-24 23:38:20', '2020-03-24 23:38:20'),
(36, 'delete_blogs', 'blogs', '2020-03-24 23:38:20', '2020-03-24 23:38:20'),
(37, 'browse_customers', 'customers', '2020-03-25 23:43:24', '2020-03-25 23:43:24'),
(38, 'read_customers', 'customers', '2020-03-25 23:43:24', '2020-03-25 23:43:24'),
(39, 'edit_customers', 'customers', '2020-03-25 23:43:24', '2020-03-25 23:43:24'),
(40, 'add_customers', 'customers', '2020-03-25 23:43:24', '2020-03-25 23:43:24'),
(41, 'delete_customers', 'customers', '2020-03-25 23:43:24', '2020-03-25 23:43:24'),
(42, 'browse_products', 'products', '2020-03-25 23:46:51', '2020-03-25 23:46:51'),
(43, 'read_products', 'products', '2020-03-25 23:46:51', '2020-03-25 23:46:51'),
(44, 'edit_products', 'products', '2020-03-25 23:46:51', '2020-03-25 23:46:51'),
(45, 'add_products', 'products', '2020-03-25 23:46:51', '2020-03-25 23:46:51'),
(46, 'delete_products', 'products', '2020-03-25 23:46:51', '2020-03-25 23:46:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(34, 2),
(35, 1),
(35, 2),
(36, 1),
(36, 2),
(37, 1),
(37, 2),
(38, 1),
(38, 2),
(39, 1),
(39, 2),
(40, 1),
(40, 2),
(41, 1),
(41, 2),
(42, 1),
(42, 2),
(43, 1),
(43, 2),
(44, 1),
(44, 2),
(45, 1),
(45, 2),
(46, 1),
(46, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kategori` tinytext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `title`, `image`, `harga`, `created_at`, `updated_at`, `kategori`, `desc`) VALUES
(1, 'Photo Print', 'products\\March2020\\Yn7TkEbD27kzWAoLz8bV.png', 'Rp.664,000', '2020-03-25 23:47:00', '2020-03-28 19:33:06', 'foto', '<p>sadsadasd <strong>inibold</strong></p>'),
(2, 'Test 2', 'products\\March2020\\hwfnFq7MKcFD4oSyBgpn.png', 'Rp.664,000', '2020-03-28 03:15:49', '2020-03-28 03:15:49', 'promosi', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-03-20 04:53:17', '2020-03-20 04:53:17'),
(2, 'user', 'Normal User', '2020-03-20 04:53:17', '2020-03-20 04:53:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Printugo', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Berdiri sejak tahun 2016 sebagai Startup yang fokus pada Periklanan Transportasi, Sticar Group berekspansi ke bisnis periklanan dan percetakan luar ruang (Outdoor Advertising) yang lebih luas dan mendirikan Printugo.', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\April2020\\loUhiZitzANINjxQTjTD.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\April2020\\CV9D68t7Bo888HD7JFYK.png', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Printugo', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Kami menyediakan platform jasa percetakan luar ruang untuk bisnis anda dengan kualitas baik dan harga terjangkau', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\April2020\\x1QsMnNB7eGHcC52Fd3H.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\April2020\\Z2Jle09lZQQaZWHa52Pz.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@database.com', 'users/default.png', NULL, '$2y$10$u7nrfULUTXmkjZFkT4iNduW4bwqXK5.9W52U3UuVpNa33Ef5Hwc2i', NULL, NULL, '2020-03-20 04:55:41', '2020-03-20 04:55:41'),
(2, 2, 'admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$XOCQT4FTbhlzY1tUWk.EJOBttK.aY6U2/KQspxcH7uctOaso7TRS.', NULL, '{\"locale\":\"en\"}', '2020-04-05 03:38:16', '2020-04-05 03:38:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(2, 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indeks untuk tabel `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indeks untuk tabel `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indeks untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indeks untuk tabel `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ketidakleluasaan untuk tabel `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
