import React, { Component } from "react";
import { Row, Col, Typography, Button, Divider } from "antd";
import { Link } from "react-router-dom";
import AOS from "aos";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import FooterMobile from "../components/FooterMobile";
import NavbarMobile from "../components/NavbarMobile";
import { DownOutlined, UserOutlined } from "@ant-design/icons";
import { inject, observer } from "mobx-react";

@inject("newsStore")
@observer
class OurCustomerDesc extends Component {
    state = {
        width: window.innerWidth,
        currentPage: 1,
        todosPerPage: 3
    };

    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
        this.props.newsStore.customer();
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    render() {
        const { width, currentPage, todosPerPage } = this.state;
        const isMobile = width <= 500;
        const { dataCustomer } = this.props.newsStore;
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = dataCustomer.slice(
            indexOfFirstTodo,
            indexOfLastTodo
        );
        const pageNumbers = [];
        for (
            let i = 1;
            i <= Math.ceil(dataCustomer.length / todosPerPage);
            i++
        ) {
            pageNumbers.push(i);
        }
        const renderPageNumbers = pageNumbers.map(number => {
            return (
                <li key={number} id={number} onClick={this.handleClick}>
                    {number}
                </li>
            );
        });
        //untuk pc//
        if (!isMobile) {
            return (
                <React.Fragment>
                    <Navbar></Navbar>
                    <Row
                        style={{
                            paddingTop: "9rem",
                            paddingLeft: "7rem",
                            paddingRight: "7rem",
                            paddingBottom: "1rem",
                            backgroundColor: "#f5f5f5"
                        }}
                    >
                        <Col
                            md={16}
                            style={{
                                paddingLeft: "8.5rem",
                                paddingBottom: "1.5rem"
                            }}
                        >
                            <img
                                src={`${window.location.origin}/image/grab1.png`}
                            />
                        </Col>
                        <Col md={8}>
                            <img
                                src={`${window.location.origin}/image/grab2.png`}
                            />
                        </Col>
                        <Col md={8} style={{ paddingLeft: "1.5rem" }}>
                            <img
                                src={`${window.location.origin}/image/grab3.png`}
                            />
                        </Col>
                        <Col md={16}>
                            <img
                                src={`${window.location.origin}/image/grab4.png`}
                            />
                        </Col>
                    </Row>
                    <Row
                        style={{
                            paddingLeft: "12rem",
                            paddingRight: "12rem",
                            paddingTop: "1rem",
                            paddingBottom: "9rem",
                            backgroundColor: "#f5f5f5"
                        }}
                    >
                        <Col md={24}>
                            <Typography
                                style={{
                                    fontWeight: "bold",
                                    fontSize: "1.3vw",
                                    paddingBottom: "0.8rem"
                                }}
                            >
                                Grab Advertising
                            </Typography>
                        </Col>
                        <Col md={24}>
                            <Typography>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Varius quis in sagittis aliquet
                                pellentesque eget ultrices tempor sed. Metus
                                massa est quis volutpat, netus praesent tellus
                                aliquet. Molestie enim mauris est etiam at
                                risus. Massa proin at sodales dolor lectus nisl
                                vel sit dictum. Fermentum sed enim nunc dui diam
                                non. Neque, eu arcu auctor donec aenean mauris.
                                Morbi sed tincidunt arcu, volutpat rhoncus
                                blandit. Sed consequat enim eget gravida aliquam
                                risus, sem. Montes, ante nisi quis urna sed
                                neque, egestas hac. Aliquet ornare sagittis
                                senectus amet, tellus sed est. Facilisi porta
                                elit, condimentum porttitor lorem. Ut. •
                                Maecenas amet tempor est • Maecenas amet tempor
                                est • Maecenas amet tempor est Maecenas amet
                                tempor est, aliquet netus gravida enim. Tortor
                                cras enim egestas vulputate vitae et, elementum
                                volutpat magna. Aliquam in arcu dictum sodales
                                porta morbi suspendisse odio. Consequat egestas
                                tincidunt vulputate hac aliquet nam purus
                                euismod. Sit viverra convallis nulla eleifend
                                netus viverra phasellus. Placerat consequat
                                lorem nibh elit est. Dui, ornare nibh adipiscing
                                duis nulla ullamcorper orci id.
                            </Typography>
                        </Col>
                    </Row>
                    <Footer />
                </React.Fragment>
            );
        } else {
            return (
                <div>
                    <NavbarMobile />
                    <Row className="blogsection1-mobile">
                        <Col
                            xs={24}
                            align="left"
                            style={{
                                paddingBottom: "1rem",
                                paddingTop: "1.5rem"
                            }}
                        >
                            <Link to="/product">
                                <img
                                    src={`${window.location.origin}/image/back.svg`}
                                />
                            </Link>
                        </Col>
                        <Col xs={24}>
                            <img
                                src={`${window.location.origin}/image/grab1.png`}
                                style={{
                                    width: "100%",
                                    paddingBottom: "1rem"
                                }}
                            />
                        </Col>
                        <Col
                            xs={24}
                            style={{
                                paddingTop: "0.5rem",
                                paddingBottom: "1rem"
                            }}
                        >
                            <Typography
                                style={{ fontSize: "5vw", fontWeight: "bold" }}
                            >
                                Grab Advertising
                            </Typography>

                            <Typography
                                style={{
                                    fontSize: "3.7vw",
                                    paddintTop: "1rem"
                                }}
                            >
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Ut sociis elit, neque,
                                dignissim id. Id faucibus sapien purus donec
                                cursus. Suspendisse et est justo neque, est
                                volutpat vestibulum faucibus. Ornare nam sed
                                velit feugiat aliquet. Sollicitudin condimentum
                                egestas gravida semper odio interdum. Aliquet
                                eu, neque imperdiet sed leo consectetur
                                bibendum.
                            </Typography>
                        </Col>
                        <Col xs={24}>
                            <img
                                src={`${window.location.origin}/image/grab2.png`}
                                style={{
                                    width: "100%",
                                    paddingBottom: "1rem"
                                }}
                            />
                        </Col>
                        <Col xs={24}>
                            <img
                                src={`${window.location.origin}/image/grab3.png`}
                                style={{
                                    width: "100%",
                                    paddingBottom: "1rem"
                                }}
                            />
                        </Col>
                        <Col
                            xs={24}
                            style={{
                                paddingBottom: "1rem"
                            }}
                        >
                            <Typography
                                style={{
                                    fontSize: "3.7vw"
                                }}
                            >
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Ut sociis elit, neque,
                                dignissim id. Id faucibus sapien purus donec
                                cursus. Suspendisse et est justo neque, est
                                volutpat vestibulum faucibus. Ornare nam sed
                                velit feugiat aliquet. Sollicitudin condimentum
                                egestas gravida semper odio interdum. Aliquet
                                eu, neque imperdiet sed leo consectetur
                                bibendum.
                            </Typography>
                        </Col>
                        <Col xs={24}>
                            <img
                                src={`${window.location.origin}/image/grab4.png`}
                                style={{
                                    width: "100%",
                                    paddingBottom: "1rem"
                                }}
                            />
                        </Col>
                    </Row>
                    <FooterMobile />
                </div>
            );
        }
    }
}

export default OurCustomerDesc;
