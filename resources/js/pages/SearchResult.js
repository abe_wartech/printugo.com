import React, { Component } from "react";
import {
    BackTop,
    Row,
    Col,
    Typography,
    Button,
    Card,
    Menu,
    Dropdown,
} from "antd";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import FooterMobile from "../components/FooterMobile";
import NavbarMobile from "../components/NavbarMobile";
import { Link } from "react-router-dom";
import AOS from "aos";
import { DownOutlined, UserOutlined } from "@ant-design/icons";
import { inject, observer } from "mobx-react";

@inject("newsStore")
@observer
class SearchResult extends Component {
    state = {
        width: window.innerWidth,
        currentPage: 1,
        todosPerPage: 3,
    };

    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50,
        });
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    render() {
        const { width, currentPage, todosPerPage } = this.state;
        const isMobile = width <= 500;
        const { dataSearch } = this.props.newsStore;
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = dataSearch.slice(
            indexOfFirstTodo,
            indexOfLastTodo
        );
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(dataSearch.length / todosPerPage); i++) {
            pageNumbers.push(i);
        }
        const renderPageNumbers = pageNumbers.map((number) => {
            return (
                <li key={number} id={number} onClick={this.handleClick}>
                    {number}
                </li>
            );
        });
        const menu = (
            <Menu>
                <Menu.Item
                    key="1"
                    onClick={(event) => handleListItemClick(event, 0)}
                >
                    <UserOutlined />
                    Foto
                </Menu.Item>
                <Menu.Item
                    key="2"
                    onClick={(event) => handleListItemClick(event, 1)}
                >
                    <UserOutlined />
                    Kebutuhan Promosi
                </Menu.Item>
                <Menu.Item
                    key="3"
                    onClick={(event) => handleListItemClick(event, 2)}
                >
                    <UserOutlined />
                    Kebutuhan Kantor
                </Menu.Item>
                <Menu.Item
                    key="4"
                    onClick={(event) => handleListItemClick(event, 3)}
                >
                    <UserOutlined />
                    Kartu Nama
                </Menu.Item>
                <Menu.Item
                    key="5"
                    onClick={(event) => handleListItemClick(event, 4)}
                >
                    <UserOutlined />
                    Packaging
                </Menu.Item>
                <Menu.Item
                    key="6"
                    onClick={(event) => handleListItemClick(event, 5)}
                >
                    <UserOutlined />
                    Merchandise
                </Menu.Item>
            </Menu>
        );
        //untuk pc//
        if (!isMobile) {
            return (
                <div>
                    <Navbar></Navbar>
                    <Row className="productsection1">
                        <Col md={5}>
                            <Typography
                                style={{
                                    fontSize: "1.5vw",
                                    marginBottom: "1rem",
                                    fontWeight: "bold",
                                }}
                            >
                                Our product
                            </Typography>
                            <Link to="">
                                <Typography style={{ marginBottom: "0.6rem" }}>
                                    Foto
                                </Typography>
                            </Link>
                            <Link to="">
                                <Typography style={{ marginBottom: "0.6rem" }}>
                                    Kebutuhan Promosi
                                </Typography>
                            </Link>
                            <Link to="">
                                <Typography style={{ marginBottom: "0.6rem" }}>
                                    Kebutuhan Kantor
                                </Typography>
                            </Link>
                            <Link to="">
                                <Typography style={{ marginBottom: "0.6rem" }}>
                                    Kartu Nama
                                </Typography>
                            </Link>
                            <Link to="">
                                <Typography style={{ marginBottom: "0.6rem" }}>
                                    Packaging
                                </Typography>
                            </Link>
                            <Link to="">
                                <Typography style={{ marginBottom: "0.6rem" }}>
                                    Merchandise
                                </Typography>
                            </Link>
                        </Col>
                        <Col md={19}>
                            <Row>
                                {currentTodos.map((item) => (
                                    <Col md={8}>
                                        <Link to="/productdetail">
                                            <Card style={{ width: "85%" }}>
                                                <img
                                                    style={{
                                                        width: "100%",
                                                        height: "auto",
                                                    }}
                                                    src={`${window.location.origin}/storage/${item.image}`}
                                                />
                                                <div className="product-list">
                                                    <Typography>
                                                        {item.title}
                                                    </Typography>
                                                    <Row>
                                                        <Col md={22}>
                                                            <Typography
                                                                style={{
                                                                    fontSize:
                                                                        "1vw",
                                                                    marginLeft:
                                                                        "0.7rem",
                                                                    fontWeight:
                                                                        "bold",
                                                                    color:
                                                                        "#2F80ED",
                                                                }}
                                                            >
                                                                {item.harga}
                                                            </Typography>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </Card>
                                        </Link>
                                    </Col>
                                ))}
                            </Row>
                            <Row style={{ marginTop: 50 }}>
                                <Col>
                                    <ul id="page-numbers">
                                        {renderPageNumbers}
                                    </ul>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Footer />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        } else {
            //untuk mobile//
            return (
                <div>
                    <NavbarMobile />
                    <Row className="productsection1-mobile">
                        <Col xs={13}>
                            <Typography
                                style={{
                                    fontSize: "4.5vw",
                                    fontWeight: "bold",
                                    paddingBottom: "2rem",
                                }}
                            >
                                Our Product
                            </Typography>
                        </Col>
                        <Col xs={11}>
                            <Dropdown overlay={menu}>
                                <Button
                                    style={{ width: "100%", textAlign: "left" }}
                                >
                                    <span style={{ paddingRight: "5rem" }}>
                                        Foto
                                    </span>
                                    <DownOutlined />
                                </Button>
                            </Dropdown>
                        </Col>
                        <Row
                            type="flex"
                            gutter={30}
                            justify="space-around"
                            data-aos="slide-right"
                        >
                            {currentTodos.map((item) => (
                                <Col
                                    xs={12}
                                    align="center"
                                    style={{
                                        paddingBottom: "1.5rem",
                                    }}
                                >
                                    <Link to={`/productdetail/${item.id}`}>
                                        <Card
                                            cover={
                                                <img
                                                    src={`${window.location.origin}/storage/${item.image}`}
                                                    style={{
                                                        width: "100%",
                                                        height: "auto",
                                                    }}
                                                />
                                            }
                                        >
                                            <div style={{ padding: "1rem" }}>
                                                <Typography
                                                    style={{
                                                        fontSize: "3.5vw",
                                                    }}
                                                >
                                                    {item.title}
                                                </Typography>
                                                <Typography
                                                    style={{
                                                        fontSize: "3.5vw",
                                                        fontWeight: "bold",
                                                        color: "#2F80ED",
                                                    }}
                                                >
                                                    {item.harga}
                                                </Typography>
                                            </div>
                                        </Card>
                                    </Link>
                                </Col>
                            ))}
                        </Row>
                        <Row style={{ marginTop: 50 }}>
                            <Col>
                                <ul id="page-numbers">{renderPageNumbers}</ul>
                            </Col>
                        </Row>
                    </Row>

                    <FooterMobile />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        }
    }
}

export default SearchResult;
