import React, { Component } from "react";
import { Row, Col, Typography, Button, Card, Menu, Dropdown } from "antd";
import { Link } from "react-router-dom";
import AOS from "aos";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import FooterMobile from "../components/FooterMobile";
import NavbarMobile from "../components/NavbarMobile";
import { DownOutlined, UserOutlined } from "@ant-design/icons";
import { inject, observer } from "mobx-react";

@inject("newsStore")
@observer
class OurCustomer extends Component {
    state = {
        width: window.innerWidth
    };

    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
        this.props.newsStore.customer();
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    handleClick = event => {
        this.setState({
            currentPage: Number(event.target.id)
        });
    };
    render() {
        const { width, currentPage, todosPerPage } = this.state;
        const isMobile = width <= 500;
        const { dataCustomer } = this.props.newsStore;
        const currentTodos = dataCustomer;

        const handleListItemClick = (event, index) => {
            if (index === 0) {
                this.props.newsStore.transport();
            }
            if (index === 1) {
                this.props.newsStore.marketing();
            }
            if (index === 2) {
                this.props.newsStore.office();
            }
            if (index === 3) {
                this.props.newsStore.signage();
            }
            if (index === 4) {
                this.props.newsStore.garmen();
            }
            if (index === 5) {
                this.props.newsStore.packagingCus();
            }
            this.setState({ selectedIndex: index });
        };
        const menu = (
            <Menu>
                <Menu.Item
                    key="1"
                    onClick={event => handleListItemClick(event, 0)}
                >
                    <UserOutlined />
                    Transport Advertising
                </Menu.Item>
                <Menu.Item
                    key="2"
                    onClick={event => handleListItemClick(event, 1)}
                >
                    <UserOutlined />
                    Marketing Prints
                </Menu.Item>
                <Menu.Item
                    key="3"
                    onClick={event => handleListItemClick(event, 2)}
                >
                    <UserOutlined />
                    Office Needs Prints
                </Menu.Item>
                <Menu.Item
                    key="4"
                    onClick={event => handleListItemClick(event, 3)}
                >
                    <UserOutlined />
                    Signage Advertising
                </Menu.Item>
                <Menu.Item
                    key="5"
                    onClick={event => handleListItemClick(event, 4)}
                >
                    <UserOutlined />
                    Garmen Prints
                </Menu.Item>
                <Menu.Item
                    key="6"
                    onClick={event => handleListItemClick(event, 5)}
                >
                    <UserOutlined />
                    Packaging Prints
                </Menu.Item>
            </Menu>
        );
        //untuk pc//
        if (!isMobile) {
            return (
                <React.Fragment>
                    <Navbar></Navbar>

                    <Row
                        className="aboutsection1"
                        type="flex"
                        align="middle"
                        justify="center"
                    >
                        <Col xs={24} md={24}>
                            <Typography
                                data-aos="zoom-in-up"
                                level={1}
                                style={{
                                    fontSize: "2.7vw",
                                    color: "white",
                                    textAlign: "center"
                                }}
                            >
                                Our Customer
                            </Typography>
                        </Col>
                    </Row>
                    <Row className="productsection1">
                        <Col md={5}>
                            <Typography
                                style={{
                                    fontSize: "1.5vw",
                                    marginBottom: "1rem",
                                    fontWeight: "bold"
                                }}
                            >
                                All Category
                            </Typography>
                            <Menu>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 0)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0,5rem" }}
                                        >
                                            Transport Advertising
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 0)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0,5rem" }}
                                        >
                                            Marketing Prints
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 0)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0,5rem" }}
                                        >
                                            Office Needs Prints
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 0)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0,5rem" }}
                                        >
                                            Signage Advertising
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 0)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0,5rem" }}
                                        >
                                            Garmen Prints
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 0)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0,5rem" }}
                                        >
                                            Packaging Prints
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                            </Menu>
                        </Col>
                        <Col md={19}>
                            <Row>
                                {currentTodos.map(item => (
                                    <Col
                                        md={8}
                                        style={{ marginBottom: "2rem" }}
                                    >
                                        <Link to="/ourcustomerdesc">
                                            <Card style={{ width: "85%" }}>
                                                <img
                                                    src={`${window.location.origin}/storage/${item.image}`}
                                                    style={{
                                                        width: "17.25vw",
                                                        height: "22vh"
                                                    }}
                                                />
                                                <div className="product-list">
                                                    <Typography>
                                                        {item.title}
                                                    </Typography>
                                                    <Row>
                                                        <Col md={22}>
                                                            <Typography
                                                                style={{
                                                                    fontSize:
                                                                        "1vw",
                                                                    marginLeft:
                                                                        "0.7rem",
                                                                    fontWeight:
                                                                        "bold",
                                                                    color:
                                                                        "#2F80ED"
                                                                }}
                                                            >
                                                                {item.harga}
                                                            </Typography>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </Card>
                                        </Link>
                                    </Col>
                                ))}
                            </Row>
                        </Col>
                    </Row>
                    <Footer />
                </React.Fragment>
            );
        } else {
            return (
                <div>
                    <NavbarMobile />

                    <Row
                        className="aboutsection1-mobile"
                        type="flex"
                        justify="center"
                    >
                        <Col xs={13}>
                            <Typography
                                style={{
                                    fontSize: "4.5vw",
                                    fontWeight: "bold",
                                    paddingBottom: "2rem"
                                }}
                            >
                                Our Customer
                            </Typography>
                        </Col>
                        <Col xs={11}>
                            <Dropdown overlay={menu}>
                                <Button
                                    style={{
                                        width: "100%",
                                        textAlign: "left",
                                        height: "5vh"
                                    }}
                                >
                                    <span style={{ paddingRight: "5rem" }}>
                                        Transport Advertising
                                    </span>
                                    <DownOutlined />
                                </Button>
                            </Dropdown>
                        </Col>
                    </Row>
                    <Row className="customersection1-mobile">
                        {currentTodos.map(item => (
                            <Col xs={24}>
                                <Link to="/ourcustomerdesc">
                                    <Card
                                        cover={
                                            <img
                                                src={`${window.location.origin}/storage/${item.image}`}
                                            />
                                        }
                                    >
                                        <div className="product-list-mobile">
                                            <Typography
                                                style={{
                                                    fontSize: "4.5vw",
                                                    fontWeight: "bold"
                                                }}
                                            >
                                                {item.title}
                                            </Typography>
                                            <Row>
                                                <Col
                                                    xs={24}
                                                    style={{
                                                        paddingTop: "0.5rem"
                                                    }}
                                                >
                                                    <Typography
                                                        style={{
                                                            fontSize: "3vw",
                                                            fontWeight: "bold"
                                                        }}
                                                    >
                                                        {item.harga}
                                                    </Typography>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Card>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                    <FooterMobile />
                </div>
            );
        }
    }
}

export default OurCustomer;
