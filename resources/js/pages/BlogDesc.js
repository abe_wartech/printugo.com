import React, { Component } from "react";
import { Row, Col, Typography } from "antd";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import FooterMobile from "../components/FooterMobile";
import NavbarMobile from "../components/NavbarMobile";
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";
import AOS from "aos";

@inject("newsStore")
@observer
class BlogDesc extends Component {
    state = {
        width: window.innerWidth,
    };

    componentDidMount = () => {
        const idUrl = window.location.hash;
        const data = {
            id: idUrl.replace("#/blogdesc/", ""),
        };
        AOS.init({
            duration: 1000,
            delay: 50,
        });
        this.props.newsStore.oneblog(data);
        this.props.newsStore.news();
    };

    componentDidUpdate = () => {
        const idUrl = window.location.hash;
        const data = {
            id: idUrl.replace("#/blogdesc/", ""),
        };
        this.props.newsStore.oneblog(data);
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    render() {
        const { width } = this.state;
        const isMobile = width <= 500;
        const { dataOneBlog, dataNews } = this.props.newsStore;
        //untuk pc//
        if (!isMobile) {
            return (
                <React.Fragment>
                    <Navbar></Navbar>
                    <Row className="blogsection" justify="space-around">
                        <Col md={24}>
                            <img
                                src={`${window.location.origin}/storage/${dataOneBlog.image}`}
                                style={{
                                    width: "100vw",
                                    paddingBottom: "1rem",
                                }}
                            />
                            <Typography
                                style={{
                                    marginTop: "-5rem",
                                    fontSize: "2.3vw",
                                    fontWeight: "bold",
                                    paddingRight: "4rem",
                                    paddingLeft: "4rem",
                                    color: "white",
                                }}
                            >
                                {dataOneBlog.resume}
                            </Typography>
                        </Col>
                        <Col
                            md={6}
                            style={{ padding: "4rem 2rem 1.5rem 4rem" }}
                        >
                            <Typography style={{ fontSize: "1.2vw" }}>
                                {dataOneBlog.title}
                            </Typography>
                        </Col>
                        <Col
                            md={18}
                            style={{
                                padding: "4rem 18rem 4.5rem 1rem",
                                textAlign: "justify",
                            }}
                        >
                            <Typography>
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: dataOneBlog.desc,
                                    }}
                                />
                            </Typography>
                        </Col>
                    </Row>

                    <Row
                        className="aboutsection2"
                        align="middle"
                        justify="space-around"
                    >
                        <Col md={24}>
                            <Typography
                                style={{
                                    fontSize: "1.6vw",
                                    fontWeight: "bold",
                                    paddingBottom: "2rem",
                                }}
                            >
                                Artikel Lainnya
                            </Typography>
                        </Col>
                        {dataNews.map((item) => (
                            <Col md={6} style={{ padding: "0.5rem" }}>
                                <Link to={`/blogdesc/${item.id}`}>
                                    <img
                                        src={`${window.location.origin}/storage/${item.image}`}
                                        style={{
                                            width: "100%",
                                            paddingBottom: "1rem",
                                        }}
                                    />
                                    <Typography style={{ fontSize: "1.5vw" }}>
                                        {item.title}
                                    </Typography>
                                    <Typography>{item.resume}</Typography>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                    <Footer />
                </React.Fragment>
            );
        } else {
            return (
                <React.Fragment>
                    <NavbarMobile />
                    <Row className="blogsection1-mobile">
                        <Col
                            xs={24}
                            align="left"
                            style={{
                                paddingBottom: "1rem",
                                paddingTop: "1.5rem",
                            }}
                        >
                            <Link to="/blog">
                                <img
                                    src={`${window.location.origin}/image/back.svg`}
                                />
                            </Link>
                        </Col>
                        <Col xs={24}>
                            <img
                                src={`${window.location.origin}/storage/${dataOneBlog.image}`}
                                style={{
                                    width: "100%",
                                    paddingBottom: "1rem",
                                }}
                            />
                            <Typography
                                style={{
                                    fontSize: "4vw",
                                    fontWeight: "bold",
                                    color: "#2f80ed",
                                }}
                            >
                                {dataOneBlog.resume}
                            </Typography>
                        </Col>
                        <Col xs={24}>
                            <Typography
                                style={{
                                    fontSize: "3vw",
                                    paddingTop: "1rem",
                                    paddingBottom: "1rem",
                                }}
                            >
                                {dataOneBlog.title}
                            </Typography>
                        </Col>
                        <Col
                            xs={24}
                            style={{
                                textAlign: "justify",
                                fontSize: "3.6vw",
                            }}
                        >
                            <Typography>
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: dataOneBlog.desc,
                                    }}
                                />
                            </Typography>
                        </Col>
                    </Row>
                    <FooterMobile />
                </React.Fragment>
            );
        }
    }
}

export default BlogDesc;
