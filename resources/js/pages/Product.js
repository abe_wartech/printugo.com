import React, { Component } from "react";
import { BackTop, Row, Col, Typography, Card, Menu, Select } from "antd";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import FooterMobile from "../components/FooterMobile";
import NavbarMobile from "../components/NavbarMobile";
import { Link } from "react-router-dom";
import AOS from "aos";
import { inject, observer } from "mobx-react";

const { Option } = Select;

@inject("newsStore")
@observer
class Product extends Component {
    state = {
        width: window.innerWidth,
        currentPage: 1,
        activeMenu: false
    };

    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
        this.props.newsStore.product();
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }
    menuActive() {
        this.setState({
            activeMenu: true
        });
    }
    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    handleClick = event => {
        this.setState({
            currentPage: Number(event.target.id)
        });
    };
    render() {
        const { width } = this.state;
        const isMobile = width <= 500;
        const { dataProduct } = this.props.newsStore;
        const currentTodos = dataProduct;

        const handleListItemClick = (event, index) => {
            if (index === 0) {
                this.props.newsStore.foto();
            }
            if (index === 1) {
                this.props.newsStore.promosi();
            }
            if (index === 2) {
                this.props.newsStore.kantor();
            }
            if (index === 3) {
                this.props.newsStore.kartunama();
            }
            if (index === 4) {
                this.props.newsStore.packaging();
            }
            if (index === 5) {
                this.props.newsStore.merchandise();
            }
            this.setState({ selectedIndex: index });
        };

        const handleChange = index => {
            if (index == 0) {
                this.props.newsStore.foto();
            }
            if (index == 1) {
                this.props.newsStore.promosi();
            }
            if (index == 2) {
                this.props.newsStore.kantor();
            }
            if (index == 3) {
                this.props.newsStore.kartunama();
            }
            if (index == 4) {
                this.props.newsStore.packaging();
            }
            if (index == 5) {
                this.props.newsStore.merchandise();
            }
        };

        //untuk pc//
        if (!isMobile) {
            return (
                <div>
                    <Navbar></Navbar>
                    <Row className="productsection1">
                        <Col md={5}>
                            <Typography
                                style={{
                                    fontSize: "1.5vw",
                                    marginBottom: "1rem",
                                    fontWeight: "bold"
                                }}
                            >
                                Our Product
                            </Typography>
                            <Menu>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 0)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0.5rem" }}
                                        >
                                            Foto
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 1)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0.5rem" }}
                                        >
                                            Kebutuhan Promosi
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 2)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0.5rem" }}
                                        >
                                            Kebutuhan Kantor
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 3)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0.5rem" }}
                                        >
                                            Kartu Nama
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 4)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0.5rem" }}
                                        >
                                            Packaging
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item
                                    className="link-category"
                                    style={{ paddingLeft: 0, marginBottom: 0 }}
                                >
                                    <Link
                                        onClick={event =>
                                            handleListItemClick(event, 5)
                                        }
                                    >
                                        <Typography
                                            style={{ marginBottom: "0.5rem" }}
                                        >
                                            Merchandise
                                        </Typography>
                                    </Link>
                                </Menu.Item>
                            </Menu>
                        </Col>
                        <Col md={19}>
                            <Row>
                                {currentTodos.map(item => (
                                    <Col
                                        md={8}
                                        style={{ marginBottom: "2rem" }}
                                    >
                                        <Link to={`/productdetail/${item.id}`}>
                                            <Card style={{ width: "85%" }}>
                                                <img
                                                    src={`${window.location.origin}/storage/${item.image}`}
                                                    style={{
                                                        width: "100%",
                                                        height: "auto"
                                                    }}
                                                />
                                                <div className="product-list">
                                                    <Typography>
                                                        {item.title}
                                                    </Typography>
                                                    <Row>
                                                        <Col md={22}>
                                                            <Typography
                                                                style={{
                                                                    fontSize:
                                                                        "1vw",
                                                                    marginLeft:
                                                                        "0.7rem",
                                                                    fontWeight:
                                                                        "bold",
                                                                    color:
                                                                        "#2F80ED"
                                                                }}
                                                            >
                                                                {item.harga}
                                                            </Typography>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </Card>
                                        </Link>
                                    </Col>
                                ))}
                            </Row>
                        </Col>
                    </Row>
                    <Footer />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        } else {
            //untuk mobile//
            return (
                <div>
                    <NavbarMobile />
                    <Row className="productsection1-mobile">
                        <Col xs={13}>
                            <Typography
                                style={{
                                    fontSize: "4.5vw",
                                    fontWeight: "bold",
                                    paddingBottom: "2rem"
                                }}
                            >
                                Our Product
                            </Typography>
                        </Col>
                        <Col xs={11}>
                            <Select
                                defaultValue="0"
                                onChange={handleChange}
                                style={{ width: "100%" }}
                            >
                                <Option value="0">Foto</Option>
                                <Option value="1">Kebutuhan Promosi</Option>
                                <Option value="2">Kebutuhan Kantor</Option>
                                <Option value="3">Kartu Nama</Option>
                                <Option value="4">Packaging</Option>
                                <Option value="5">Merchandise</Option>
                            </Select>
                        </Col>
                        <Row
                            type="flex"
                            gutter={30}
                            justify="space-around"
                            data-aos="slide-right"
                        >
                            {currentTodos.map(item => (
                                <Col
                                    xs={12}
                                    align="center"
                                    style={{
                                        paddingBottom: "1.5rem"
                                    }}
                                >
                                    <Link to={`/productdetail/${item.id}`}>
                                        <Card
                                            cover={
                                                <img
                                                    src={`${window.location.origin}/storage/${item.image}`}
                                                    style={{
                                                        width: "100%",
                                                        height: "auto"
                                                    }}
                                                />
                                            }
                                        >
                                            <div style={{ padding: "1rem" }}>
                                                <Typography
                                                    style={{
                                                        fontSize: "3.5vw"
                                                    }}
                                                >
                                                    {item.title}
                                                </Typography>
                                                <Typography
                                                    style={{
                                                        fontSize: "3.5vw",
                                                        fontWeight: "bold",
                                                        color: "#2F80ED"
                                                    }}
                                                >
                                                    {item.harga}
                                                </Typography>
                                            </div>
                                        </Card>
                                    </Link>
                                </Col>
                            ))}
                        </Row>
                    </Row>

                    <FooterMobile />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        }
    }
}

export default Product;
