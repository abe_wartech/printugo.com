import React, { Component } from "react";
import { BackTop, Row, Col, Typography, Button, Divider } from "antd";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import FooterMobile from "../components/FooterMobile";
import { Link } from "react-router-dom";
import NavbarMobile from "../components/NavbarMobile";
import AOS from "aos";
import { inject, observer } from "mobx-react";

@inject("newsStore")
@observer
class Home extends Component {
    state = {
        width: window.innerWidth
    };

    componentDidMount = () => {
        const idUrl = window.location.hash;
        const data = {
            id: idUrl.replace("#/productdetail/", "")
        };
        AOS.init({
            duration: 1000,
            delay: 50
        });
        this.props.newsStore.onearticle(data);
    };

    componentDidUpdate = () => {
        const idUrl = window.location.hash;
        const data = {
            id: idUrl.replace("#/productdetail/", "")
        };
        this.props.newsStore.onearticle(data);
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    render() {
        const { width } = this.state;
        const isMobile = width <= 500;
        const { dataOneProduct } = this.props.newsStore;
        //untuk pc//
        if (!isMobile) {
            return (
                <div>
                    <Navbar></Navbar>
                    <Row align="middle" className="productdetailsection">
                        <Col md={14}>
                            <img
                                src={`${window.location.origin}/storage/${dataOneProduct.image}`}
                                style={{
                                    width: "100%",
                                    height: "auto"
                                }}
                            />
                        </Col>
                        <Col md={10} style={{ paddingLeft: "2rem" }}>
                            <Typography
                                style={{
                                    fontSize: "1.5vw",
                                    fontWeight: "bold"
                                }}
                            >
                                {dataOneProduct.title}
                            </Typography>
                            <Divider />
                            <Typography>{dataOneProduct.harga}</Typography>
                            <Typography>
                                Price includes GST and Shipping
                            </Typography>
                            <Typography style={{ paddingBottom: "2rem" }}>
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: dataOneProduct.desc
                                    }}
                                />
                            </Typography>
                            <Button
                                type="primary"
                                block
                                style={{
                                    height: "6vh"
                                }}
                                href="https://wa.wizard.id/00bdac"
                                target="_blank"
                                size="large"
                            >
                                <Typography>Order Now </Typography>
                            </Button>
                        </Col>
                    </Row>
                    <Footer />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        } else {
            //untuk mobile//
            return (
                <div>
                    <NavbarMobile />
                    <Row className="blogsection1-mobile">
                        <Col
                            xs={24}
                            align="left"
                            style={{
                                paddingBottom: "1rem",
                                paddingTop: "1.5rem"
                            }}
                        >
                            <Link to="/product">
                                <img
                                    src={`${window.location.origin}/image/back.svg`}
                                />
                            </Link>
                        </Col>
                        <Col xs={24}>
                            <img
                                src={`${window.location.origin}/storage/${dataOneProduct.image}`}
                                style={{
                                    width: "100%",
                                    paddingBottom: "1rem"
                                }}
                            />
                        </Col>
                        <Col
                            xs={24}
                            style={{
                                paddingTop: "0.5rem",
                                paddingBottom: "2rem"
                            }}
                        >
                            <Typography
                                style={{ fontSize: "5vw", fontWeight: "bold" }}
                            >
                                {dataOneProduct.title}
                            </Typography>
                            <Divider
                                style={{
                                    height: "2px",
                                    width: "100%",
                                    background: "#afafaf"
                                }}
                            />
                            <Typography
                                style={{
                                    fontSize: "4.8vw",
                                    fontWeight: "bold"
                                }}
                            >
                                {dataOneProduct.harga}
                            </Typography>
                            <Typography
                                style={{
                                    fontSize: "3.9vw",
                                    fontWeight: "bold",
                                    paddingBottom: "1rem",
                                    color: "#afafaf"
                                }}
                            >
                                Price includes GST and Shipping
                            </Typography>
                            <Typography style={{ paddingBottom: "2rem" }}>
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: dataOneProduct.desc
                                    }}
                                />
                            </Typography>
                            <Button
                                type="primary"
                                block
                                style={{
                                    height: "8vh"
                                }}
                            >
                                <a
                                    style={{
                                        color: "white",
                                        fontWeight: "bold",
                                        fontSize: "4.5vw"
                                    }}
                                    href="https://wa.wizard.id/00bdac"
                                    target="_blank"
                                >
                                    Order Now
                                </a>
                            </Button>
                        </Col>
                    </Row>
                    <FooterMobile />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        }
    }
}

export default Home;
