import React, { Component } from "react";
import { Row, Col, Typography } from "antd";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import FooterMobile from "../components/FooterMobile";
import NavbarMobile from "../components/NavbarMobile";
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";
import AOS from "aos";

@inject("newsStore")
@observer
class Home extends Component {
    state = {
        width: window.innerWidth,
        currentPage: 1,
        todosPerPage: 3,
    };

    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50,
        });
        this.props.newsStore.news();
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    render() {
        const { width, currentPage, todosPerPage } = this.state;
        const isMobile = width <= 500;
        const { dataNews } = this.props.newsStore;
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = dataNews.slice(indexOfFirstTodo, indexOfLastTodo);
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(dataNews.length / todosPerPage); i++) {
            pageNumbers.push(i);
        }
        const renderPageNumbers = pageNumbers.map((number) => {
            return (
                <li key={number} id={number} onClick={this.handleClick}>
                    {number}
                </li>
            );
        });
        //untuk pc//
        if (!isMobile) {
            return (
                <React.Fragment>
                    <Navbar></Navbar>
                    <Row
                        className="aboutsection1"
                        type="flex"
                        justify="space-around"
                        align="middle"
                    >
                        <Col xs={24} md={24}>
                            <Typography
                                data-aos="zoom-in-up"
                                level={1}
                                style={{
                                    fontSize: "2.7vw",
                                    color: "white",
                                    textAlign: "center",
                                }}
                            >
                                Blog dan Media
                            </Typography>
                        </Col>
                    </Row>
                    <Row
                        className="aboutsection2"
                        align="middle"
                        justify="space-around"
                    >
                        {currentTodos.map((item) => (
                            <Col md={8} style={{ padding: "0.5rem" }}>
                                <Link to={`/blogdesc/${item.id}`}>
                                    <img
                                        src={`${window.location.origin}/storage/${item.image}`}
                                        style={{
                                            width: "100%",
                                            paddingBottom: "1rem",
                                        }}
                                    />
                                    <Typography style={{ fontSize: "1.5vw" }}>
                                        {item.title}
                                    </Typography>
                                    <Typography>{item.resume}</Typography>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                    <Row>
                        <Col>
                            <ul id="page-numbers">{renderPageNumbers}</ul>
                        </Col>
                    </Row>
                    <Footer />
                </React.Fragment>
            );
        } else {
            return (
                <React.Fragment>
                    <NavbarMobile />
                    <Row
                        style={{
                            paddingLeft: "1rem",
                            paddingRight: "1rem",
                            paddingTop: "6rem",
                            paddingBottom: "6rem",
                            backgroundColor: "#f5f5f5",
                        }}
                        type="flex"
                        justify="space-around"
                        gutter={20}
                    >
                        {currentTodos.map((item) => (
                            <Col xs={24} md={24}>
                                <Link to={`/blogdesc/${item.id}`}>
                                    <div className="blog">
                                        <img
                                            src={`${window.location.origin}/storage/${item.image}`}
                                            style={{
                                                width: "40vw",
                                                height: "15vh",
                                                paddingBottom: "1rem",
                                            }}
                                        />
                                    </div>
                                    <div className="blogdesc">
                                        <Typography
                                            style={{
                                                fontSize: "4.5vw",
                                                fontWeight: "bold",
                                            }}
                                        >
                                            {item.title}
                                        </Typography>
                                        <Typography
                                            style={{
                                                fontSize: "2.5vw",
                                                paddingTop: "0.5rem",
                                            }}
                                        >
                                            {item.resume}
                                        </Typography>
                                    </div>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                    <FooterMobile />
                </React.Fragment>
            );
        }
    }
}

export default Home;
