import React, { Component } from "react";
import { BackTop, Row, Col, Typography, Card, Divider, Input } from "antd";
import Footer from "../components/Footer";
import FooterMobile from "../components/FooterMobile";
import Navbar from "../components/Navbar";
import NavbarMobile from "../components/NavbarMobile";
import Section1 from "../components/Beranda/Section1";
import Section2 from "../components/Beranda/Section2";
import Section3 from "../components/Beranda/Section3";
import Section4 from "../components/Beranda/Section4";
import AOS from "aos";
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";

const { Search } = Input;

@inject("newsStore")
@observer
class Home extends Component {
    state = {
        width: window.innerWidth,
        searchText: ""
    };

    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
        this.props.newsStore.homeproduct();
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    handleSearch = e => {
        e.preventDefault();
        const data = {
            q: this.state.searchText
        };
        this.props.newsStore.search(data);
        this.props.history.push("/search");
    };

    render() {
        const { width } = this.state;
        const isMobile = width <= 500;
        const { dataProduct } = this.props.newsStore;
        //untuk pc//
        if (!isMobile) {
            return (
                <div>
                    <Navbar></Navbar>
                    <Section1 />
                    <Section2 />
                    <Section3 />
                    <Section4 />
                    <Footer />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        } else {
            //untuk mobile//
            return (
                <div>
                    <NavbarMobile />
                    <Row
                        className="section1-mobile"
                        type="flex"
                        justify="center"
                        align="middle"
                    >
                        <Col xs={24}>
                            <form onSubmit={this.handleSearch}>
                                <Search
                                    placeholder="Cari Produk"
                                    style={{
                                        width: "100%",
                                        height: "6vh",
                                        padding: "0.7rem 1rem"
                                    }}
                                    onChange={e =>
                                        this.setState({
                                            searchText: e.target.value
                                        })
                                    }
                                />
                            </form>
                        </Col>
                    </Row>
                    <Row
                        align="middle"
                        style={{ padding: "2rem 1rem 2.5rem 1rem" }}
                    >
                        <Col xs={6} align="center">
                            <img
                                src={`${window.location.origin}/image/start-mobile.svg`}
                            />
                            <Typography style={{ fontSize: "3vw" }}>
                                Kualitas Terbaik
                            </Typography>
                        </Col>
                        <Col xs={6} align="center">
                            <img
                                src={`${window.location.origin}/image/guarentee-mobile.svg`}
                            />
                            <Typography
                                style={{
                                    fontSize: "3vw"
                                }}
                            >
                                Bergaransi
                            </Typography>
                        </Col>
                        <Col xs={6} align="center">
                            <img
                                src={`${window.location.origin}/image/cheaper-mobile.svg`}
                            />
                            <Typography style={{ fontSize: "3vw" }}>
                                Harga Terjangkau
                            </Typography>
                        </Col>
                        <Col xs={6} align="center">
                            <img
                                src={`${window.location.origin}/image/free-ong-mobile.svg`}
                            />
                            <Typography style={{ fontSize: "3vw" }}>
                                Gratis Pengiriman
                            </Typography>
                        </Col>
                    </Row>
                    <Row
                        style={{
                            backgroundColor: "#f5f5f5",
                            paddingTop: "2rem",
                            paddingBottom: "3.5rem",
                            paddingRight: "1.5rem",
                            paddingLeft: "1.5rem"
                        }}
                        align="middle"
                    >
                        <Col xs={24} align="center" justify="center">
                            <img
                                src={`${window.location.origin}/image/logo-mobile.svg`}
                            />
                        </Col>
                        <Col xs={24} align="left">
                            <Typography
                                style={{
                                    fontSize: "3.5vw",
                                    color: "black",
                                    textAlign: "justify"
                                }}
                            >
                                Berdiri sejak tahun 2016 sebagai Startup yang
                                fokus pada Periklanan Transportasi, Sticar
                                Group berekspansi ke bisnis periklanan dan
                                percetakan luar ruang (Outdoor Advertising) yang
                                lebih luas dan mendirikan Printugo.
                                <br />
                                <br />
                                Kami menyediakan platform jasa percetakan luar
                                ruang untuk bisnis anda dengan kualitas baik dan
                                harga terjangkau
                            </Typography>
                        </Col>
                    </Row>
                    <Row className="section2-mobile" justify="flex-start">
                        <Typography
                            style={{
                                fontWeight: "bold",
                                fontSize: "4.5vw",
                                color: "white"
                            }}
                        >
                            Proses Pemesanan
                        </Typography>
                        <Col
                            data-aos="slide-right"
                            data-aos-delay="550"
                            xs={24}
                            align="left"
                            style={{
                                paddingTop: "1rem",
                                paddingBottom: "1rem"
                            }}
                        >
                            <img
                                src={`${window.location.pathname}image/timeline-mobile.png`}
                            />
                        </Col>
                    </Row>
                    <Row className="section3-mobile" justify="center">
                        <Col
                            style={{
                                paddingBottom: "1.5rem"
                            }}
                            data-aos="slide-right"
                            data-aos-delay="550"
                            xs={24}
                            align="center"
                        >
                            <Typography
                                style={{
                                    fontSize: "4.5vw",
                                    fontWeight: "bold",
                                    paddingBottom: "1rem"
                                }}
                            >
                                Our Product
                            </Typography>
                            <Divider />
                        </Col>
                        <Row
                            type="flex"
                            gutter={30}
                            justify="space-around"
                            data-aos="slide-right"
                        >
                            {dataProduct.map(item => (
                                <Col
                                    xs={12}
                                    align="center"
                                    style={{
                                        paddingBottom: "1.5rem"
                                    }}
                                >
                                    <Link to={`/productdetail/${item.id}`}>
                                        <Card
                                            cover={
                                                <img
                                                    src={`${window.location.origin}/storage/${item.image}`}
                                                    style={{
                                                        width: "100%",
                                                        height: "auto"
                                                    }}
                                                />
                                            }
                                        >
                                            <div style={{ padding: "1rem" }}>
                                                <Typography
                                                    style={{
                                                        fontSize: "3.5vw"
                                                    }}
                                                >
                                                    {item.title}
                                                </Typography>
                                                <Typography
                                                    style={{
                                                        fontSize: "3.5vw",
                                                        fontWeight: "bold",
                                                        color: "#2F80ED"
                                                    }}
                                                >
                                                    {item.harga}
                                                </Typography>
                                            </div>
                                        </Card>
                                    </Link>
                                </Col>
                            ))}
                        </Row>
                    </Row>
                    <Row className="section4-mobile" gutter={20}>
                        <Col
                            data-aos="slide-right"
                            data-aos-delay="550"
                            xs={{ span: 24 }}
                            align="center"
                            style={{ paddingBottom: "1.5rem" }}
                        >
                            <Typography
                                style={{
                                    fontSize: "4.5vw",
                                    fontWeight: "bold",
                                    paddingBottom: "1rem"
                                }}
                            >
                                Apa Kata Mereka
                            </Typography>
                            <Divider />
                        </Col>
                        <Col
                            xs={{ span: 12, offset: 1 }}
                            sm={{ span: 12, offset: 1 }}
                            className="section4-left"
                        >
                            <img
                                src={`${window.location.origin}/image/foto-mas.png`}
                            />
                        </Col>
                        <Col
                            xs={{ span: 11 }}
                            sm={{ span: 11 }}
                            className="section4-right"
                        >
                            <Typography style={{ fontSize: "3.5vw" }}>
                                <span style={{ color: "#2F80ED" }}>
                                    Affan Awangga
                                </span>
                                <span style={{ fontWeight: "bold" }}>
                                    - OYO Rooms
                                </span>
                            </Typography>
                            <Typography
                                style={{
                                    fontSize: "3vw",
                                    paddingTop: "1rem"
                                }}
                            >
                                "Kalau soal kecepatan dan kualitas Sticar nomor
                                satu. Hasilnya juga memuaskan. Kita pesan
                                Signage untuk OYO di 100 titik dan selesai
                                tempat waktu."
                            </Typography>
                        </Col>
                    </Row>
                    <FooterMobile />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        }
    }
}

export default Home;
