import React, { Component } from "react";
import { Row, Col, Typography, Divider } from "antd";
import AOS from "aos";

class FooterMobile extends Component {
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };
    render() {
        return (
            <>
                <Row className="section9-mobile" justify="center">
                    <Col xs={24} align="center">
                        <img
                            src={`${window.location.origin}/image/footer-logo.svg`}
                        />
                    </Col>
                    <Col xs={24} style={{ paddingTop: "2rem" }}>
                        <Row>
                            <Col xs={3}>
                                <div className="icon">
                                    <img
                                        src={`${window.location.origin}/image/location.svg`}
                                    />
                                </div>
                            </Col>
                            <Col xs={21}>
                                <Typography className="footer-contact-mobile">
                                    Printugo
                                    <br />
                                    Jl. Kemang Timur No 12, Mampang Prapatan,
                                    Jakarta Selatan
                                </Typography>
                            </Col>
                            <Col xs={3}>
                                <div className="icon">
                                    <img
                                        src={`${window.location.origin}/image/email.svg`}
                                    />
                                </div>
                            </Col>
                            <Col xs={21}>
                                <Typography className="footer-contact-mobile">
                                    hello@printugo.co.id
                                </Typography>
                            </Col>
                            <Col xs={3}>
                                <div className="icon">
                                    <img
                                        src={`${window.location.origin}/image/phone.svg`}
                                    />
                                </div>
                            </Col>
                            <Col xs={21}>
                                <Typography className="footer-contact-mobile">
                                    +62 822 2132 3726
                                </Typography>
                            </Col>
                        </Row>
                    </Col>
                    <br />
                    <Divider
                        style={{
                            height: "2px",
                            width: "100%",
                            background: "#afafaf"
                        }}
                    />
                    <Typography> Copyright @ 2020 Printugo</Typography>
                </Row>
                <Row justify="center" style={{ paddingBottom: 10 }}>
                    <Typography>PT. Optimal Digital Solusi</Typography>
                </Row>
            </>
        );
    }
}
export default FooterMobile;
