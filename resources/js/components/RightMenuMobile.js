import React, { Component } from "react";
import { Menu, Typography } from "antd";
import { Link } from "react-router-dom";

import {
    LinkedinFilled,
    InstagramFilled,
    FacebookFilled
} from "@ant-design/icons";

class RightMenuMobile extends Component {
    render() {
        return (
            <div>
                <div
                    style={{
                        backgroundColor: "white",
                        padding: "0.2rem",
                        fontSize: "4vw",
                        textAlign: "center"
                    }}
                >
                    <img
                        src={`${window.location.origin}/image/logo-mobile.svg`}
                        style={{ width: "20vw" }}
                    />
                </div>
                <Menu
                    style={{ paddingTop: "1rem", paddingBottom: "50%" }}
                    defaultOpenKeys={["sub1"]}
                    mode="inline"
                >
                    <Menu.Item key="sub1">
                        <Link
                            to="/"
                            style={{ color: "white", fontSize: "4.5vw" }}
                        >
                            Home
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="sub2">
                        <Link
                            to="/product"
                            style={{ color: "white", fontSize: "4.5vw" }}
                        >
                            Product
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="sub3">
                        <Link
                            to="/ourcustomer"
                            style={{ color: "white", fontSize: "4.5vw" }}
                        >
                            Our Customer
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="sub4">
                        <Link
                            to="/blog"
                            style={{ color: "white", fontSize: "4.5vw" }}
                        >
                            Blog
                        </Link>
                    </Menu.Item>
                </Menu>
                <div align="center" className="container-icon">
                    <a
                        href="https://www.linkedin.com/company/printugo"
                        target="_blank"
                    >
                        <LinkedinFilled className="icon-sosmed" />
                    </a>
                    <a
                        href="https://www.instagram.com/printugo/"
                        target="_blank"
                    >
                        <InstagramFilled className="icon-sosmed" />
                    </a>
                    <a
                        href="https://www.facebook.com/Printugo-108425620748937/"
                        target="_blank"
                    >
                        <FacebookFilled className="icon-sosmed" />
                    </a>
                </div>
            </div>
        );
    }
}

export default RightMenuMobile;
