import React, { Component } from "react";
import { Row, Col, Typography, Card } from "antd";
import AOS from "aos";
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";

const { Title } = Typography;

@inject("newsStore")
@observer
class Section3 extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false,
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50,
        });
    };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true,
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    render() {
        const { dataProduct } = this.props.newsStore;
        return (
            <div>
                <Row className="section3" justify="center">
                    <Col
                        data-aos="slide-right"
                        data-aos-delay="550"
                        xs={24}
                        align="left"
                    >
                        <Title
                            level={4}
                            className="text-white"
                            style={{ fontSize: "1.9vw", fontWeight: "bold" }}
                        >
                            Our Product
                        </Title>
                    </Col>
                    <Row
                        type="flex"
                        gutter={48}
                        justify="space-around"
                        data-aos="slide-right"
                    >
                        {dataProduct.map((item) => (
                            <Col
                                md={8}
                                align="center"
                                style={{ paddingBottom: "2.5rem" }}
                            >
                                <Link to={`/productdetail/${item.id}`}>
                                    <Card
                                        style={{
                                            width: "27vw",
                                        }}
                                        cover={
                                            <img
                                                src={`${window.location.origin}/storage/${item.image}`}
                                                style={{
                                                    width: "100%",
                                                    height: "auto",
                                                }}
                                            />
                                        }
                                    >
                                        <Typography className="title-invest">
                                            {item.title}
                                        </Typography>
                                        <div className="padding-invest">
                                            <Typography>
                                                Kami menyediakan jasa periklanan
                                                di transportasi mobil bagi anda
                                                yang ingin mengenalkan brand
                                                anda.
                                            </Typography>
                                            <Row
                                                style={{ paddingTop: "1.5rem" }}
                                            >
                                                <Col md={2} className="float">
                                                    <img
                                                        src={`${window.location.origin}/image/label.svg`}
                                                    />
                                                </Col>
                                                <Col md={22}>
                                                    <Typography
                                                        style={{
                                                            fontSize: "1.5vw",
                                                            marginLeft:
                                                                "0.7rem",
                                                            fontWeight: "bold",
                                                            color: "#2F80ED",
                                                        }}
                                                    >
                                                        {item.harga}
                                                    </Typography>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Card>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                </Row>
            </div>
        );
    }
}
export default Section3;
