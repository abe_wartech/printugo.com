import React, { Component } from "react";
import { Row, Col, Typography } from "antd";
import AOS from "aos";

class Section4 extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };

    showModal = () => {
        this.setState({
            visible: true
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false
        });
    };
    render() {
        const { visible, confirmLoading, width } = this.state;
        return (
            <div>
                <Row className="section4" justify="center" align="middle">
                    <Col
                        data-aos="slide-right"
                        data-aos-delay="550"
                        md={5}
                        align="left"
                        style={{ marginBottom: 30 }}
                    >
                        <Typography
                            style={{ fontSize: "1.5vw", fontWeight: "bold" }}
                        >
                            Apa Kata Mereka
                        </Typography>
                    </Col>
                    <Col md={19} data-aos="slide-left" data-aos-delay="550">
                        <Row align="middle">
                            <Col md={8}>
                                <img
                                    src={`${window.location.origin}/image/picture-testimonial.png`}
                                />
                            </Col>
                            <Col md={16}>
                                <Typography style={{ fontSize: "1.5vw" }}>
                                    <span style={{ color: "#2F80ED" }}>
                                        Affan Awangga
                                    </span>
                                    <span style={{ fontWeight: "bold" }}>
                                        {" "}
                                        - OYO Rooms
                                    </span>
                                </Typography>
                                <Typography
                                    style={{
                                        fontSize: "1.5vw",
                                        paddingTop: "4rem",
                                        paddingBottom: "4rem"
                                    }}
                                >
                                    "Kalau soal kecepatan dan kualitas Sticar
                                    nomor satu. Hasilnya juga memuaskan. Kita
                                    pesan Signage untuk OYO di 100 titik dan
                                    selesai tempat waktu."
                                </Typography>
                                <img
                                    src={`${window.location.origin}/image/oyo-logo.svg`}
                                />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default Section4;
