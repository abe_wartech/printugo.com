import React, { Component } from "react";
import { Row, Col, Typography, Input } from "antd";
import { withRouter } from "react-router-dom";
import AOS from "aos";
import { inject, observer } from "mobx-react";

const { Search } = Input;

@inject("newsStore")
@observer
class Section1 extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false,
        searchText: ""
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };

    showModal = () => {
        this.setState({
            visible: true
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false
        });
    };
    handleSearch = e => {
        e.preventDefault();
        const data = {
            q: this.state.searchText
        };
        this.props.newsStore.search(data);
        this.props.history.push("/search");
    };
    render() {
        return (
            <div>
                <Row
                    className="section1"
                    type="flex"
                    justify="center"
                    align="middle"
                >
                    <Col xs={24} md={24} style={{ textAlign: "center" }}>
                        <form onSubmit={this.handleSearch}>
                            <Search
                                placeholder="Cari Produk"
                                style={{
                                    width: "80%",
                                    height: "8vh",
                                    padding: "0.9rem"
                                }}
                                onChange={e =>
                                    this.setState({
                                        searchText: e.target.value
                                    })
                                }
                            />
                        </form>
                    </Col>
                </Row>
                <Row align="middle" className="top">
                    <Col md={6} align="center">
                        <img
                            src={`${window.location.origin}/image/star-icon.svg`}
                        />
                        <Typography style={{ fontSize: "1.1vw" }}>
                            Kualitas Terbaik
                        </Typography>
                    </Col>
                    <Col md={6} align="center">
                        <img
                            src={`${window.location.origin}/image/guarentee.svg`}
                        />
                        <Typography style={{ fontSize: "1.1vw" }}>
                            Bergaransi
                        </Typography>
                    </Col>
                    <Col md={6} align="center">
                        <img
                            src={`${window.location.origin}/image/lower-price.svg`}
                        />
                        <Typography style={{ fontSize: "1.1vw" }}>
                            Harga Terjangkau
                        </Typography>
                    </Col>
                    <Col md={6} align="center">
                        <img
                            src={`${window.location.origin}/image/expedition.svg`}
                        />
                        <Typography style={{ fontSize: "1.1vw" }}>
                            Gratis Pengiriman
                        </Typography>
                    </Col>
                </Row>
                <Row
                    style={{
                        backgroundColor: "#f5f5f5",
                        paddingTop: "7rem",
                        paddingBottom: "7rem",
                        paddingRight: "10rem",
                        paddingLeft: "10rem"
                    }}
                    justify="center"
                    align="middle"
                >
                    <Col md={8} align="left" justify="center">
                        <img
                            src={`${window.location.origin}/image/logo-printugo.svg`}
                        />
                    </Col>
                    <Col md={16} align="left">
                        <Typography
                            style={{ fontSize: "1.1vw", color: "black" }}
                        >
                            Berdiri sejak tahun 2016 sebagai Startup yang fokus
                            pada Periklanan Transportasi, Sticar
                            Group berekspansi ke bisnis periklanan dan
                            percetakan luar ruang (Outdoor Advertising) yang
                            lebih luas dan mendirikan Printugo.
                            <br />
                            <br />
                            Kami menyediakan platform jasa percetakan luar ruang
                            untuk bisnis anda dengan kualitas baik dan harga
                            terjangkau
                        </Typography>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default withRouter(Section1);
