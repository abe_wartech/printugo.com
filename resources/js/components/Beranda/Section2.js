import React, { Component } from "react";
import { Row, Col, Typography } from "antd";
import AOS from "aos";

const { Title } = Typography;

class Section2 extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };

    showModal = () => {
        this.setState({
            visible: true
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false
        });
    };
    render() {
        const { visible, confirmLoading, width } = this.state;
        return (
            <div>
                <Row className="section2" justify="center">
                    <Col
                        data-aos="slide-right"
                        data-aos-delay="550"
                        xs={24}
                        align="center"
                    >
                        <img
                            src={`${window.location.pathname}image/timeline.png`}
                            className="img1"
                        />
                    </Col>
                </Row>
            </div>
        );
    }
}
export default Section2;
