import React, { Component } from "react";
import RightMenuMobile from "./RightMenuMobile";
import { Drawer, Button, Col, Row } from "antd";

class NavbarMobile extends Component {
    state = {
        current: "sub1",
        visible: false
    };
    showDrawer = () => {
        this.setState({
            visible: true
        });
    };

    onClose = () => {
        this.setState({
            visible: false
        });
    };

    render() {
        return (
            <Row align="middle" className="row-navbar-mobile">
                <Col xs={20}>
                    <div className="logo">
                        <a href="/">
                            <img
                                src={`${window.location.origin}/image/logo-navbar.svg`}
                            />
                        </a>
                    </div>
                </Col>
                <Col xs={4}>
                    <div className="menuCon">
                        <Button
                            className="barsMenu"
                            type="primary"
                            onClick={this.showDrawer}
                        >
                            <span className="barsBtn"></span>
                        </Button>
                        <Drawer
                            style={{ color: "#23ad46" }}
                            placement="right"
                            closable={false}
                            onClose={this.onClose}
                            visible={this.state.visible}
                        >
                            <RightMenuMobile />
                        </Drawer>
                    </div>
                </Col>
            </Row>
        );
    }
}

export default NavbarMobile;
