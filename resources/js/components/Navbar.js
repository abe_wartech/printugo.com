import React, { Component } from "react";
import RightMenu from "./RightMenu";
import { Drawer, Button, Row, Col } from "antd";

class Navbar extends Component {
    state = {
        current: "mail",
        visible: false
    };
    showDrawer = () => {
        this.setState({
            visible: true
        });
    };

    onClose = () => {
        this.setState({
            visible: false
        });
    };

    render() {
        return (
            <Row align="middle" className="row-navbar">
                <Col md={12}>
                    <div className="logo">
                        <a href="">
                            <img
                                src={`${window.location.origin}/image/printugo-logo.svg`}
                            />
                        </a>
                    </div>
                </Col>
                <Col md={12}>
                    <div className="menuCon">
                        <div className="rightMenu">
                            <RightMenu />
                        </div>
                    </div>
                </Col>
            </Row>
        );
    }
}

export default Navbar;
