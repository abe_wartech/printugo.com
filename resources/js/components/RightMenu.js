import React, { Component } from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import {
    LinkedinFilled,
    InstagramFilled,
    FacebookFilled
} from "@ant-design/icons";

class RightMenu extends Component {
    render() {
        let href = window.location.href.split("/");
        href = href[4];
        return (
            <Menu
                className="menu-antd"
                mode="horizontal"
                defaultSelectedKeys={["/" + href]}
                selectedKeys={["/" + href]}
            >
                <Menu.Item key="/">
                    <Link to="/" className="styling">
                        Home
                    </Link>
                </Menu.Item>
                <Menu.Item key="/product">
                    <Link to="/product" className="styling">
                        Product
                    </Link>
                </Menu.Item>
                <Menu.Item key="/ourcustomer">
                    <Link to="/ourcustomer" className="styling">
                        Our Customer
                    </Link>
                </Menu.Item>
                <Menu.Item key="/blog">
                    <Link to="/blog" className="styling">
                        Blog
                    </Link>
                </Menu.Item>

                <Menu.Item key="help">
                    <a
                        href="https://www.linkedin.com/company/printugo"
                        target="_blank"
                    >
                        <LinkedinFilled
                            style={{ fontSize: "1.4vw", color: "white" }}
                        />
                    </a>
                </Menu.Item>
                <Menu.Item key="sign">
                    <a
                        href="https://www.instagram.com/printugo/"
                        target="_blank"
                    >
                        <InstagramFilled
                            style={{ fontSize: "1.4vw", color: "white" }}
                        />
                    </a>
                </Menu.Item>
                <Menu.Item key="login">
                    <a
                        href="https://www.facebook.com/Printugo-108425620748937/"
                        target="_blank"
                    >
                        <FacebookFilled
                            style={{ fontSize: "1.4vw", color: "white" }}
                        />
                    </a>
                </Menu.Item>
            </Menu>
        );
    }
}

export default RightMenu;
