import React, { Component } from "react";
import { Row, Col, Typography, Divider } from "antd";
import AOS from "aos";
import { Link } from "react-router-dom";

class Footer extends Component {
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };
    render() {
        return (
            <>
                <Row className="section9" justify="center">
                    <Col md={6} align="center">
                        <img
                            src={`${window.location.origin}/image/printugo-footer.svg`}
                        />
                    </Col>
                    <Col md={10}>
                        <div className="icon">
                            <img
                                src={`${window.location.origin}/image/location.svg`}
                            />
                        </div>
                        <Typography className="footer-contact">
                            Printugo
                            <br />
                            Jl. Kemang Timur No 12, Mampang Prapatan, Jakarta
                            Selatan
                        </Typography>
                        <div className="icon">
                            <img
                                src={`${window.location.origin}/image/email.svg`}
                            />
                        </div>

                        <Typography className="footer-contact">
                            hello@printugo.co.id
                        </Typography>
                        <div className="icon">
                            <img
                                src={`${window.location.origin}/image/phone.svg`}
                            />
                        </div>
                        <Typography className="footer-contact">
                            +62 822 2132 3726
                        </Typography>
                    </Col>
                    <Col md={4}>
                        <Link to="/product">
                            <Typography
                                style={{
                                    marginBottom: "0.8rem"
                                }}
                            >
                                Product
                            </Typography>
                        </Link>
                        <Link to="/blog">
                            <Typography
                                style={{
                                    marginBottom: "0.8rem"
                                }}
                            >
                                Blog
                            </Typography>
                        </Link>
                    </Col>
                    <Col md={4}>
                        <Link to="ourcustomer">
                            <Typography
                                style={{
                                    marginBottom: "0.8rem"
                                }}
                            >
                                Portofolio
                            </Typography>
                        </Link>
                        <Link to="/">
                            <Typography
                                style={{
                                    marginBottom: "0.8rem"
                                }}
                            >
                                About Us
                            </Typography>
                        </Link>
                    </Col>
                    <br />
                    <Divider
                        style={{
                            height: "2px",
                            width: "100%",
                            background: "#afafaf"
                        }}
                    />
                    <Typography> Copyright @ 2020 Printugo</Typography>
                </Row>
                <Row justify="center" style={{ paddingBottom: 10 }}>
                    <Typography>PT. Optimal Digital Solusi</Typography>
                </Row>
            </>
        );
    }
}
export default Footer;
