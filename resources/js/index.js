import React, { Component } from "react";
import { Switch, HashRouter, Route } from "react-router-dom";
import Home from "./pages/Home";
import Product from "./pages/Product";
import OurCustomer from "./pages/OurCustomer";
import OurCustomerDesc from "./pages/OurCustomerDesc";
import Blog from "./pages/Blog";
import BlogDesc from "./pages/BlogDesc";
import ProductDetail from "./pages/ProductDetail";
import ScrollToTop from "./ScrollToTop";
import SearchResult from "./pages/SearchResult";
import "aos/dist/aos.css";
import "antd/dist/antd.less";
import "./App.scss";

class App extends Component {
    render() {
        return (
            <HashRouter>
                <ScrollToTop />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/product" component={Product} />
                    <Route exact path="/ourcustomer" component={OurCustomer} />
                    <Route
                        exact
                        path="/ourcustomerdesc"
                        component={OurCustomerDesc}
                    />
                    <Route exact path="/blog" component={Blog} />
                    <Route exact path="/blogdesc/:id" component={BlogDesc} />
                    <Route
                        exact
                        path="/productdetail/:id"
                        component={ProductDetail}
                    />
                    <Route exact path="/search" component={SearchResult} />
                </Switch>
            </HashRouter>
        );
    }
}

export default App;
