import React from "react";
import ReactDOM from "react-dom";
import App from "./index";
import { Provider } from "mobx-react";
import newsStore from "./store/newsStore";

const stores = {
    newsStore
};

ReactDOM.render(
    <Provider {...stores}>
        <App />
    </Provider>,
    document.getElementById("app")
);
