import React, { Component } from "react";
import { observable, action, runInAction } from "mobx";

class NewsStore extends Component {
    @observable dataNews = [];
    @observable dataCustomer = [];
    @observable dataProduct = [];
    @observable dataOneProduct = [];
    @observable dataOneBlog = [];
    @observable dataHome = [];
    @observable dataSearch = [];

    @action
    news() {
        fetch("api/blog", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataNews = res.message;
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    customer() {
        fetch("api/customer", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataCustomer = res.message.filter(
                            item => item.kategori === "transport"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    product() {
        fetch("api/product", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataProduct = res.message.filter(
                            item => item.kategori === "foto"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    allproduct() {
        fetch("api/product", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataProduct = res.message;
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    homeproduct() {
        fetch("api/homeproduct", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataProduct = res.message;
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    home() {
        fetch("api/home", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataHome = res.message;
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    search(data) {
        fetch("api/search", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataSearch = res.message;
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    promosi() {
        fetch("api/product", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataProduct = res.message.filter(
                            item => item.kategori === "promosi"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    foto() {
        fetch("api/product", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataProduct = res.message.filter(
                            item => item.kategori === "foto"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    kantor() {
        fetch("api/product", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataProduct = res.message.filter(
                            item => item.kategori === "kantor"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    kartunama() {
        fetch("api/product", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataProduct = res.message.filter(
                            item => item.kategori === "kartu"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    packaging() {
        fetch("api/product", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataProduct = res.message.filter(
                            item => item.kategori === "pack"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    merchandise() {
        fetch("api/product", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataProduct = res.message.filter(
                            item => item.kategori === "merc"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    onearticle(data) {
        fetch("api/oneproduct", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataOneProduct = res.message[0];
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    oneblog(data) {
        fetch("api/oneblog", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataOneBlog = res.message[0];
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    transport() {
        fetch("api/customer", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataCustomer = res.message.filter(
                            item => item.kategori === "transport"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    marketing() {
        fetch("api/customer", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataCustomer = res.message.filter(
                            item => item.kategori === "marketing"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    office() {
        fetch("api/customer", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataCustomer = res.message.filter(
                            item => item.kategori === "office"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    signage() {
        fetch("api/customer", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataCustomer = res.message.filter(
                            item => item.kategori === "signage"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    garmen() {
        fetch("api/customer", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataCustomer = res.message.filter(
                            item => item.kategori === "garmen"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    packagingCus() {
        fetch("api/customer", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataCustomer = res.message.filter(
                            item => item.kategori === "packaging"
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }
}

export default new NewsStore();
