<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no" />
    <meta name="theme-color" content="#000000">
    <base href="/" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="Printugo" />
    <meta name="subject" content="Printugo">
    <meta name="copyright" content="Printugo">
    <meta name="robots" content="index,follow" />
    <meta name="author" content="abe, yusufarifin89@gmail.com">
    <meta name="google-site-verification" content="v1JPynR6U0XwddU9fhSmgCO1cl1Ve-1cC-2uV4Nt-Sg" />
    <meta name="url" content="https://printugo.com">
    <meta property="og:title" content="Printugo - Print Online / Cetak Online">
    <meta property="og:description" content="Printugo adalah platform online yang memudahkan urusan printing kamu dan bisnismu jadi lebih mudah.">
    <meta name="description" content="Printugo adalah platform online yang memudahkan urusan printing kamu dan bisnismu jadi lebih mudah." />

    <title>Printugo - Print Online / Cetak Online</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="{{asset('favicon.ico')}}" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ])!!};
    </script>
</head>

<body>
    <noscript>
      You need to enable JavaScript to run this app.
    </noscript>
    <div id="app">
        @yield('content')
    </div>

    <script src="{{mix('/js/app.js')}}"></script>
    <script src="{{mix('/js/manifest.js')}}"></script>
    <script src="{{mix('/js/vendor.js')}}"></script>
</body>

</html>
