<?php

Route::get('blog', 'ApiController@blog');
Route::get('customer', 'ApiController@customer');
Route::get('product', 'ApiController@product');
Route::get('homeproduct', 'ApiController@homeproduct');
Route::post('search', 'ApiController@search');
Route::post('oneproduct', 'ApiController@oneproduct');
Route::post('oneblog', 'ApiController@oneblog');
