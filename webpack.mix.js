const mix = require("laravel-mix");

mix.react("resources/js/app.js", "public/js")
    .sass("resources/sass/app.scss", "public/css")
    .extract();
mix.browserSync("localhost:8000");
if (mix.inProduction()) {
    mix.version();
}
mix.disableNotifications();
