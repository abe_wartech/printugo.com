(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/app"],{

/***/ "./node_modules/css-loader/index.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./resources/js/App.scss":
/*!******************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./resources/js/App.scss ***!
  \******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* stylelint-disable at-rule-empty-line-before,at-rule-name-space-after,at-rule-no-unknown */\n/* stylelint-disable no-duplicate-selectors */\n/* stylelint-disable */\n/* stylelint-disable declaration-bang-space-before,no-duplicate-selectors,string-no-newline */\n/* stylelint-disable at-rule-no-unknown */\nhtml,\nbody {\n  width: 100%;\n  height: 100%;\n}\n\ninput::-ms-clear,\ninput::-ms-reveal {\n  display: none;\n}\n\n*,\n*::before,\n*::after {\n  box-sizing: border-box;\n}\n\nhtml {\n  font-family: sans-serif;\n  line-height: 1.15;\n  -webkit-text-size-adjust: 100%;\n  -ms-text-size-adjust: 100%;\n  -ms-overflow-style: scrollbar;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\n}\n\n@-ms-viewport {\n  width: device-width;\n}\narticle,\naside,\ndialog,\nfigcaption,\nfigure,\nfooter,\nheader,\nhgroup,\nmain,\nnav,\nsection {\n  display: block;\n}\n\nbody {\n  margin: 0;\n  color: rgba(0, 0, 0, 0.65);\n  font-size: 14px;\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, \"Noto Sans\", sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\", \"Noto Color Emoji\";\n  font-variant: tabular-nums;\n  line-height: 1.5715;\n  background-color: #fff;\n  font-feature-settings: \"tnum\";\n}\n\n[tabindex=\"-1\"]:focus {\n  outline: none !important;\n}\n\nhr {\n  box-sizing: content-box;\n  height: 0;\n  overflow: visible;\n}\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  margin-top: 0;\n  margin-bottom: 0.5em;\n  color: rgba(0, 0, 0, 0.85);\n  font-weight: 500;\n}\n\np {\n  margin-top: 0;\n  margin-bottom: 1em;\n}\n\nabbr[title],\nabbr[data-original-title] {\n  text-decoration: underline;\n  -webkit-text-decoration: underline dotted;\n  text-decoration: underline dotted;\n  border-bottom: 0;\n  cursor: help;\n}\n\naddress {\n  margin-bottom: 1em;\n  font-style: normal;\n  line-height: inherit;\n}\n\ninput[type=text],\ninput[type=password],\ninput[type=number],\ntextarea {\n  -webkit-appearance: none;\n}\n\nol,\nul,\ndl {\n  margin-top: 0;\n  margin-bottom: 1em;\n}\n\nol ol,\nul ul,\nol ul,\nul ol {\n  margin-bottom: 0;\n}\n\ndt {\n  font-weight: 500;\n}\n\ndd {\n  margin-bottom: 0.5em;\n  margin-left: 0;\n}\n\nblockquote {\n  margin: 0 0 1em;\n}\n\ndfn {\n  font-style: italic;\n}\n\nb,\nstrong {\n  font-weight: bolder;\n}\n\nsmall {\n  font-size: 80%;\n}\n\nsub,\nsup {\n  position: relative;\n  font-size: 75%;\n  line-height: 0;\n  vertical-align: baseline;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\nsup {\n  top: -0.5em;\n}\n\na {\n  color: #1890ff;\n  text-decoration: none;\n  background-color: transparent;\n  outline: none;\n  cursor: pointer;\n  transition: color 0.3s;\n  -webkit-text-decoration-skip: objects;\n}\n\na:hover {\n  color: #40a9ff;\n}\n\na:active {\n  color: #096dd9;\n}\n\na:active,\na:hover {\n  text-decoration: none;\n  outline: 0;\n}\n\na[disabled] {\n  color: rgba(0, 0, 0, 0.25);\n  cursor: not-allowed;\n  pointer-events: none;\n}\n\npre,\ncode,\nkbd,\nsamp {\n  font-size: 1em;\n  font-family: \"SFMono-Regular\", Consolas, \"Liberation Mono\", Menlo, Courier, monospace;\n}\n\npre {\n  margin-top: 0;\n  margin-bottom: 1em;\n  overflow: auto;\n}\n\nfigure {\n  margin: 0 0 1em;\n}\n\nimg {\n  vertical-align: middle;\n  border-style: none;\n}\n\nsvg:not(:root) {\n  overflow: hidden;\n}\n\na,\narea,\nbutton,\n[role=button],\ninput:not([type=range]),\nlabel,\nselect,\nsummary,\ntextarea {\n  touch-action: manipulation;\n}\n\ntable {\n  border-collapse: collapse;\n}\n\ncaption {\n  padding-top: 0.75em;\n  padding-bottom: 0.3em;\n  color: rgba(0, 0, 0, 0.45);\n  text-align: left;\n  caption-side: bottom;\n}\n\nth {\n  text-align: inherit;\n}\n\ninput,\nbutton,\nselect,\noptgroup,\ntextarea {\n  margin: 0;\n  color: inherit;\n  font-size: inherit;\n  font-family: inherit;\n  line-height: inherit;\n}\n\nbutton,\ninput {\n  overflow: visible;\n}\n\nbutton,\nselect {\n  text-transform: none;\n}\n\nbutton,\nhtml [type=button],\n[type=reset],\n[type=submit] {\n  -webkit-appearance: button;\n}\n\nbutton::-moz-focus-inner,\n[type=button]::-moz-focus-inner,\n[type=reset]::-moz-focus-inner,\n[type=submit]::-moz-focus-inner {\n  padding: 0;\n  border-style: none;\n}\n\ninput[type=radio],\ninput[type=checkbox] {\n  box-sizing: border-box;\n  padding: 0;\n}\n\ninput[type=date],\ninput[type=time],\ninput[type=datetime-local],\ninput[type=month] {\n  -webkit-appearance: listbox;\n}\n\ntextarea {\n  overflow: auto;\n  resize: vertical;\n}\n\nfieldset {\n  min-width: 0;\n  margin: 0;\n  padding: 0;\n  border: 0;\n}\n\nlegend {\n  display: block;\n  width: 100%;\n  max-width: 100%;\n  margin-bottom: 0.5em;\n  padding: 0;\n  color: inherit;\n  font-size: 1.5em;\n  line-height: inherit;\n  white-space: normal;\n}\n\nprogress {\n  vertical-align: baseline;\n}\n\n[type=number]::-webkit-inner-spin-button,\n[type=number]::-webkit-outer-spin-button {\n  height: auto;\n}\n\n[type=search] {\n  outline-offset: -2px;\n  -webkit-appearance: none;\n}\n\n[type=search]::-webkit-search-cancel-button,\n[type=search]::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\n\n::-webkit-file-upload-button {\n  font: inherit;\n  -webkit-appearance: button;\n}\n\noutput {\n  display: inline-block;\n}\n\nsummary {\n  display: list-item;\n}\n\ntemplate {\n  display: none;\n}\n\n[hidden] {\n  display: none !important;\n}\n\nmark {\n  padding: 0.2em;\n  background-color: #feffe6;\n}\n\n::-moz-selection {\n  color: #fff;\n  background: #1890ff;\n}\n\n::selection {\n  color: #fff;\n  background: #1890ff;\n}\n\n.clearfix::before {\n  display: table;\n  content: \"\";\n}\n\n.clearfix::after {\n  display: block;\n  clear: both;\n  content: \"\";\n}\n\n.anticon {\n  display: inline-block;\n  color: inherit;\n  font-style: normal;\n  line-height: 0;\n  text-align: center;\n  text-transform: none;\n  vertical-align: -0.125em;\n  text-rendering: optimizeLegibility;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n}\n\n.anticon > * {\n  line-height: 1;\n}\n\n.anticon svg {\n  display: inline-block;\n}\n\n.anticon::before {\n  display: none;\n}\n\n.anticon .anticon-icon {\n  display: block;\n}\n\n.anticon[tabindex] {\n  cursor: pointer;\n}\n\n.anticon-spin::before {\n  display: inline-block;\n  -webkit-animation: loadingCircle 1s infinite linear;\n  animation: loadingCircle 1s infinite linear;\n}\n\n.anticon-spin {\n  display: inline-block;\n  -webkit-animation: loadingCircle 1s infinite linear;\n  animation: loadingCircle 1s infinite linear;\n}\n\n.fade-enter,\n.fade-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.fade-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.fade-enter.fade-enter-active,\n.fade-appear.fade-appear-active {\n  -webkit-animation-name: antFadeIn;\n  animation-name: antFadeIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.fade-leave.fade-leave-active {\n  -webkit-animation-name: antFadeOut;\n  animation-name: antFadeOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.fade-enter,\n.fade-appear {\n  opacity: 0;\n  -webkit-animation-timing-function: linear;\n  animation-timing-function: linear;\n}\n\n.fade-leave {\n  -webkit-animation-timing-function: linear;\n  animation-timing-function: linear;\n}\n\n@-webkit-keyframes antFadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n@keyframes antFadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antFadeOut {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n  }\n}\n@keyframes antFadeOut {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n  }\n}\n.move-up-enter,\n.move-up-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.move-up-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.move-up-enter.move-up-enter-active,\n.move-up-appear.move-up-appear-active {\n  -webkit-animation-name: antMoveUpIn;\n  animation-name: antMoveUpIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.move-up-leave.move-up-leave-active {\n  -webkit-animation-name: antMoveUpOut;\n  animation-name: antMoveUpOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.move-up-enter,\n.move-up-appear {\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.move-up-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.6, 0.04, 0.98, 0.34);\n  animation-timing-function: cubic-bezier(0.6, 0.04, 0.98, 0.34);\n}\n\n.move-down-enter,\n.move-down-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.move-down-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.move-down-enter.move-down-enter-active,\n.move-down-appear.move-down-appear-active {\n  -webkit-animation-name: antMoveDownIn;\n  animation-name: antMoveDownIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.move-down-leave.move-down-leave-active {\n  -webkit-animation-name: antMoveDownOut;\n  animation-name: antMoveDownOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.move-down-enter,\n.move-down-appear {\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.move-down-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.6, 0.04, 0.98, 0.34);\n  animation-timing-function: cubic-bezier(0.6, 0.04, 0.98, 0.34);\n}\n\n.move-left-enter,\n.move-left-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.move-left-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.move-left-enter.move-left-enter-active,\n.move-left-appear.move-left-appear-active {\n  -webkit-animation-name: antMoveLeftIn;\n  animation-name: antMoveLeftIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.move-left-leave.move-left-leave-active {\n  -webkit-animation-name: antMoveLeftOut;\n  animation-name: antMoveLeftOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.move-left-enter,\n.move-left-appear {\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.move-left-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.6, 0.04, 0.98, 0.34);\n  animation-timing-function: cubic-bezier(0.6, 0.04, 0.98, 0.34);\n}\n\n.move-right-enter,\n.move-right-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.move-right-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.move-right-enter.move-right-enter-active,\n.move-right-appear.move-right-appear-active {\n  -webkit-animation-name: antMoveRightIn;\n  animation-name: antMoveRightIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.move-right-leave.move-right-leave-active {\n  -webkit-animation-name: antMoveRightOut;\n  animation-name: antMoveRightOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.move-right-enter,\n.move-right-appear {\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.move-right-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.6, 0.04, 0.98, 0.34);\n  animation-timing-function: cubic-bezier(0.6, 0.04, 0.98, 0.34);\n}\n\n@-webkit-keyframes antMoveDownIn {\n  0% {\n    transform: translateY(100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n  100% {\n    transform: translateY(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n}\n@keyframes antMoveDownIn {\n  0% {\n    transform: translateY(100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n  100% {\n    transform: translateY(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antMoveDownOut {\n  0% {\n    transform: translateY(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n  100% {\n    transform: translateY(100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n}\n@keyframes antMoveDownOut {\n  0% {\n    transform: translateY(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n  100% {\n    transform: translateY(100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antMoveLeftIn {\n  0% {\n    transform: translateX(-100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n  100% {\n    transform: translateX(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n}\n@keyframes antMoveLeftIn {\n  0% {\n    transform: translateX(-100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n  100% {\n    transform: translateX(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antMoveLeftOut {\n  0% {\n    transform: translateX(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n  100% {\n    transform: translateX(-100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n}\n@keyframes antMoveLeftOut {\n  0% {\n    transform: translateX(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n  100% {\n    transform: translateX(-100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antMoveRightIn {\n  0% {\n    transform: translateX(100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n  100% {\n    transform: translateX(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n}\n@keyframes antMoveRightIn {\n  0% {\n    transform: translateX(100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n  100% {\n    transform: translateX(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antMoveRightOut {\n  0% {\n    transform: translateX(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n  100% {\n    transform: translateX(100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n}\n@keyframes antMoveRightOut {\n  0% {\n    transform: translateX(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n  100% {\n    transform: translateX(100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antMoveUpIn {\n  0% {\n    transform: translateY(-100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n  100% {\n    transform: translateY(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n}\n@keyframes antMoveUpIn {\n  0% {\n    transform: translateY(-100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n  100% {\n    transform: translateY(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antMoveUpOut {\n  0% {\n    transform: translateY(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n  100% {\n    transform: translateY(-100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n}\n@keyframes antMoveUpOut {\n  0% {\n    transform: translateY(0%);\n    transform-origin: 0 0;\n    opacity: 1;\n  }\n  100% {\n    transform: translateY(-100%);\n    transform-origin: 0 0;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes loadingCircle {\n  100% {\n    transform: rotate(360deg);\n  }\n}\n@keyframes loadingCircle {\n  100% {\n    transform: rotate(360deg);\n  }\n}\n[ant-click-animating=true],\n[ant-click-animating-without-extra-node=true] {\n  position: relative;\n}\n\nhtml {\n  --antd-wave-shadow-color: #1890ff;\n}\n\n[ant-click-animating-without-extra-node=true]::after,\n.ant-click-animating-node {\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  display: block;\n  border-radius: inherit;\n  box-shadow: 0 0 0 0 #1890ff;\n  box-shadow: 0 0 0 0 var(--antd-wave-shadow-color);\n  opacity: 0.2;\n  -webkit-animation: fadeEffect 2s cubic-bezier(0.08, 0.82, 0.17, 1), waveEffect 0.4s cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation: fadeEffect 2s cubic-bezier(0.08, 0.82, 0.17, 1), waveEffect 0.4s cubic-bezier(0.08, 0.82, 0.17, 1);\n  -webkit-animation-fill-mode: forwards;\n  animation-fill-mode: forwards;\n  content: \"\";\n  pointer-events: none;\n}\n\n@-webkit-keyframes waveEffect {\n  100% {\n    box-shadow: 0 0 0 #1890ff;\n    box-shadow: 0 0 0 6px var(--antd-wave-shadow-color);\n  }\n}\n@keyframes waveEffect {\n  100% {\n    box-shadow: 0 0 0 #1890ff;\n    box-shadow: 0 0 0 6px var(--antd-wave-shadow-color);\n  }\n}\n@-webkit-keyframes fadeEffect {\n  100% {\n    opacity: 0;\n  }\n}\n@keyframes fadeEffect {\n  100% {\n    opacity: 0;\n  }\n}\n.slide-up-enter,\n.slide-up-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.slide-up-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.slide-up-enter.slide-up-enter-active,\n.slide-up-appear.slide-up-appear-active {\n  -webkit-animation-name: antSlideUpIn;\n  animation-name: antSlideUpIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.slide-up-leave.slide-up-leave-active {\n  -webkit-animation-name: antSlideUpOut;\n  animation-name: antSlideUpOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.slide-up-enter,\n.slide-up-appear {\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.23, 1, 0.32, 1);\n  animation-timing-function: cubic-bezier(0.23, 1, 0.32, 1);\n}\n\n.slide-up-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n  animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n}\n\n.slide-down-enter,\n.slide-down-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.slide-down-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.slide-down-enter.slide-down-enter-active,\n.slide-down-appear.slide-down-appear-active {\n  -webkit-animation-name: antSlideDownIn;\n  animation-name: antSlideDownIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.slide-down-leave.slide-down-leave-active {\n  -webkit-animation-name: antSlideDownOut;\n  animation-name: antSlideDownOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.slide-down-enter,\n.slide-down-appear {\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.23, 1, 0.32, 1);\n  animation-timing-function: cubic-bezier(0.23, 1, 0.32, 1);\n}\n\n.slide-down-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n  animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n}\n\n.slide-left-enter,\n.slide-left-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.slide-left-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.slide-left-enter.slide-left-enter-active,\n.slide-left-appear.slide-left-appear-active {\n  -webkit-animation-name: antSlideLeftIn;\n  animation-name: antSlideLeftIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.slide-left-leave.slide-left-leave-active {\n  -webkit-animation-name: antSlideLeftOut;\n  animation-name: antSlideLeftOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.slide-left-enter,\n.slide-left-appear {\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.23, 1, 0.32, 1);\n  animation-timing-function: cubic-bezier(0.23, 1, 0.32, 1);\n}\n\n.slide-left-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n  animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n}\n\n.slide-right-enter,\n.slide-right-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.slide-right-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.slide-right-enter.slide-right-enter-active,\n.slide-right-appear.slide-right-appear-active {\n  -webkit-animation-name: antSlideRightIn;\n  animation-name: antSlideRightIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.slide-right-leave.slide-right-leave-active {\n  -webkit-animation-name: antSlideRightOut;\n  animation-name: antSlideRightOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.slide-right-enter,\n.slide-right-appear {\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.23, 1, 0.32, 1);\n  animation-timing-function: cubic-bezier(0.23, 1, 0.32, 1);\n}\n\n.slide-right-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n  animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);\n}\n\n@-webkit-keyframes antSlideUpIn {\n  0% {\n    transform: scaleY(0.8);\n    transform-origin: 0% 0%;\n    opacity: 0;\n  }\n  100% {\n    transform: scaleY(1);\n    transform-origin: 0% 0%;\n    opacity: 1;\n  }\n}\n@keyframes antSlideUpIn {\n  0% {\n    transform: scaleY(0.8);\n    transform-origin: 0% 0%;\n    opacity: 0;\n  }\n  100% {\n    transform: scaleY(1);\n    transform-origin: 0% 0%;\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antSlideUpOut {\n  0% {\n    transform: scaleY(1);\n    transform-origin: 0% 0%;\n    opacity: 1;\n  }\n  100% {\n    transform: scaleY(0.8);\n    transform-origin: 0% 0%;\n    opacity: 0;\n  }\n}\n@keyframes antSlideUpOut {\n  0% {\n    transform: scaleY(1);\n    transform-origin: 0% 0%;\n    opacity: 1;\n  }\n  100% {\n    transform: scaleY(0.8);\n    transform-origin: 0% 0%;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antSlideDownIn {\n  0% {\n    transform: scaleY(0.8);\n    transform-origin: 100% 100%;\n    opacity: 0;\n  }\n  100% {\n    transform: scaleY(1);\n    transform-origin: 100% 100%;\n    opacity: 1;\n  }\n}\n@keyframes antSlideDownIn {\n  0% {\n    transform: scaleY(0.8);\n    transform-origin: 100% 100%;\n    opacity: 0;\n  }\n  100% {\n    transform: scaleY(1);\n    transform-origin: 100% 100%;\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antSlideDownOut {\n  0% {\n    transform: scaleY(1);\n    transform-origin: 100% 100%;\n    opacity: 1;\n  }\n  100% {\n    transform: scaleY(0.8);\n    transform-origin: 100% 100%;\n    opacity: 0;\n  }\n}\n@keyframes antSlideDownOut {\n  0% {\n    transform: scaleY(1);\n    transform-origin: 100% 100%;\n    opacity: 1;\n  }\n  100% {\n    transform: scaleY(0.8);\n    transform-origin: 100% 100%;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antSlideLeftIn {\n  0% {\n    transform: scaleX(0.8);\n    transform-origin: 0% 0%;\n    opacity: 0;\n  }\n  100% {\n    transform: scaleX(1);\n    transform-origin: 0% 0%;\n    opacity: 1;\n  }\n}\n@keyframes antSlideLeftIn {\n  0% {\n    transform: scaleX(0.8);\n    transform-origin: 0% 0%;\n    opacity: 0;\n  }\n  100% {\n    transform: scaleX(1);\n    transform-origin: 0% 0%;\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antSlideLeftOut {\n  0% {\n    transform: scaleX(1);\n    transform-origin: 0% 0%;\n    opacity: 1;\n  }\n  100% {\n    transform: scaleX(0.8);\n    transform-origin: 0% 0%;\n    opacity: 0;\n  }\n}\n@keyframes antSlideLeftOut {\n  0% {\n    transform: scaleX(1);\n    transform-origin: 0% 0%;\n    opacity: 1;\n  }\n  100% {\n    transform: scaleX(0.8);\n    transform-origin: 0% 0%;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antSlideRightIn {\n  0% {\n    transform: scaleX(0.8);\n    transform-origin: 100% 0%;\n    opacity: 0;\n  }\n  100% {\n    transform: scaleX(1);\n    transform-origin: 100% 0%;\n    opacity: 1;\n  }\n}\n@keyframes antSlideRightIn {\n  0% {\n    transform: scaleX(0.8);\n    transform-origin: 100% 0%;\n    opacity: 0;\n  }\n  100% {\n    transform: scaleX(1);\n    transform-origin: 100% 0%;\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antSlideRightOut {\n  0% {\n    transform: scaleX(1);\n    transform-origin: 100% 0%;\n    opacity: 1;\n  }\n  100% {\n    transform: scaleX(0.8);\n    transform-origin: 100% 0%;\n    opacity: 0;\n  }\n}\n@keyframes antSlideRightOut {\n  0% {\n    transform: scaleX(1);\n    transform-origin: 100% 0%;\n    opacity: 1;\n  }\n  100% {\n    transform: scaleX(0.8);\n    transform-origin: 100% 0%;\n    opacity: 0;\n  }\n}\n.zoom-enter,\n.zoom-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-enter.zoom-enter-active,\n.zoom-appear.zoom-appear-active {\n  -webkit-animation-name: antZoomIn;\n  animation-name: antZoomIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.zoom-leave.zoom-leave-active {\n  -webkit-animation-name: antZoomOut;\n  animation-name: antZoomOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.zoom-enter,\n.zoom-appear {\n  transform: scale(0);\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.zoom-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n  animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n}\n\n.zoom-big-enter,\n.zoom-big-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-big-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-big-enter.zoom-big-enter-active,\n.zoom-big-appear.zoom-big-appear-active {\n  -webkit-animation-name: antZoomBigIn;\n  animation-name: antZoomBigIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.zoom-big-leave.zoom-big-leave-active {\n  -webkit-animation-name: antZoomBigOut;\n  animation-name: antZoomBigOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.zoom-big-enter,\n.zoom-big-appear {\n  transform: scale(0);\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.zoom-big-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n  animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n}\n\n.zoom-big-fast-enter,\n.zoom-big-fast-appear {\n  -webkit-animation-duration: 0.1s;\n  animation-duration: 0.1s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-big-fast-leave {\n  -webkit-animation-duration: 0.1s;\n  animation-duration: 0.1s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-big-fast-enter.zoom-big-fast-enter-active,\n.zoom-big-fast-appear.zoom-big-fast-appear-active {\n  -webkit-animation-name: antZoomBigIn;\n  animation-name: antZoomBigIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.zoom-big-fast-leave.zoom-big-fast-leave-active {\n  -webkit-animation-name: antZoomBigOut;\n  animation-name: antZoomBigOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.zoom-big-fast-enter,\n.zoom-big-fast-appear {\n  transform: scale(0);\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.zoom-big-fast-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n  animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n}\n\n.zoom-up-enter,\n.zoom-up-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-up-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-up-enter.zoom-up-enter-active,\n.zoom-up-appear.zoom-up-appear-active {\n  -webkit-animation-name: antZoomUpIn;\n  animation-name: antZoomUpIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.zoom-up-leave.zoom-up-leave-active {\n  -webkit-animation-name: antZoomUpOut;\n  animation-name: antZoomUpOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.zoom-up-enter,\n.zoom-up-appear {\n  transform: scale(0);\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.zoom-up-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n  animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n}\n\n.zoom-down-enter,\n.zoom-down-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-down-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-down-enter.zoom-down-enter-active,\n.zoom-down-appear.zoom-down-appear-active {\n  -webkit-animation-name: antZoomDownIn;\n  animation-name: antZoomDownIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.zoom-down-leave.zoom-down-leave-active {\n  -webkit-animation-name: antZoomDownOut;\n  animation-name: antZoomDownOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.zoom-down-enter,\n.zoom-down-appear {\n  transform: scale(0);\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.zoom-down-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n  animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n}\n\n.zoom-left-enter,\n.zoom-left-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-left-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-left-enter.zoom-left-enter-active,\n.zoom-left-appear.zoom-left-appear-active {\n  -webkit-animation-name: antZoomLeftIn;\n  animation-name: antZoomLeftIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.zoom-left-leave.zoom-left-leave-active {\n  -webkit-animation-name: antZoomLeftOut;\n  animation-name: antZoomLeftOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.zoom-left-enter,\n.zoom-left-appear {\n  transform: scale(0);\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.zoom-left-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n  animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n}\n\n.zoom-right-enter,\n.zoom-right-appear {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-right-leave {\n  -webkit-animation-duration: 0.2s;\n  animation-duration: 0.2s;\n  -webkit-animation-fill-mode: both;\n  animation-fill-mode: both;\n  -webkit-animation-play-state: paused;\n  animation-play-state: paused;\n}\n\n.zoom-right-enter.zoom-right-enter-active,\n.zoom-right-appear.zoom-right-appear-active {\n  -webkit-animation-name: antZoomRightIn;\n  animation-name: antZoomRightIn;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n}\n\n.zoom-right-leave.zoom-right-leave-active {\n  -webkit-animation-name: antZoomRightOut;\n  animation-name: antZoomRightOut;\n  -webkit-animation-play-state: running;\n  animation-play-state: running;\n  pointer-events: none;\n}\n\n.zoom-right-enter,\n.zoom-right-appear {\n  transform: scale(0);\n  opacity: 0;\n  -webkit-animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n  animation-timing-function: cubic-bezier(0.08, 0.82, 0.17, 1);\n}\n\n.zoom-right-leave {\n  -webkit-animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n  animation-timing-function: cubic-bezier(0.78, 0.14, 0.15, 0.86);\n}\n\n@-webkit-keyframes antZoomIn {\n  0% {\n    transform: scale(0.2);\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    opacity: 1;\n  }\n}\n@keyframes antZoomIn {\n  0% {\n    transform: scale(0.2);\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antZoomOut {\n  0% {\n    transform: scale(1);\n  }\n  100% {\n    transform: scale(0.2);\n    opacity: 0;\n  }\n}\n@keyframes antZoomOut {\n  0% {\n    transform: scale(1);\n  }\n  100% {\n    transform: scale(0.2);\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antZoomBigIn {\n  0% {\n    transform: scale(0.8);\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    opacity: 1;\n  }\n}\n@keyframes antZoomBigIn {\n  0% {\n    transform: scale(0.8);\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    opacity: 1;\n  }\n}\n@-webkit-keyframes antZoomBigOut {\n  0% {\n    transform: scale(1);\n  }\n  100% {\n    transform: scale(0.8);\n    opacity: 0;\n  }\n}\n@keyframes antZoomBigOut {\n  0% {\n    transform: scale(1);\n  }\n  100% {\n    transform: scale(0.8);\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antZoomUpIn {\n  0% {\n    transform: scale(0.8);\n    transform-origin: 50% 0%;\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    transform-origin: 50% 0%;\n  }\n}\n@keyframes antZoomUpIn {\n  0% {\n    transform: scale(0.8);\n    transform-origin: 50% 0%;\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    transform-origin: 50% 0%;\n  }\n}\n@-webkit-keyframes antZoomUpOut {\n  0% {\n    transform: scale(1);\n    transform-origin: 50% 0%;\n  }\n  100% {\n    transform: scale(0.8);\n    transform-origin: 50% 0%;\n    opacity: 0;\n  }\n}\n@keyframes antZoomUpOut {\n  0% {\n    transform: scale(1);\n    transform-origin: 50% 0%;\n  }\n  100% {\n    transform: scale(0.8);\n    transform-origin: 50% 0%;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antZoomLeftIn {\n  0% {\n    transform: scale(0.8);\n    transform-origin: 0% 50%;\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    transform-origin: 0% 50%;\n  }\n}\n@keyframes antZoomLeftIn {\n  0% {\n    transform: scale(0.8);\n    transform-origin: 0% 50%;\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    transform-origin: 0% 50%;\n  }\n}\n@-webkit-keyframes antZoomLeftOut {\n  0% {\n    transform: scale(1);\n    transform-origin: 0% 50%;\n  }\n  100% {\n    transform: scale(0.8);\n    transform-origin: 0% 50%;\n    opacity: 0;\n  }\n}\n@keyframes antZoomLeftOut {\n  0% {\n    transform: scale(1);\n    transform-origin: 0% 50%;\n  }\n  100% {\n    transform: scale(0.8);\n    transform-origin: 0% 50%;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antZoomRightIn {\n  0% {\n    transform: scale(0.8);\n    transform-origin: 100% 50%;\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    transform-origin: 100% 50%;\n  }\n}\n@keyframes antZoomRightIn {\n  0% {\n    transform: scale(0.8);\n    transform-origin: 100% 50%;\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    transform-origin: 100% 50%;\n  }\n}\n@-webkit-keyframes antZoomRightOut {\n  0% {\n    transform: scale(1);\n    transform-origin: 100% 50%;\n  }\n  100% {\n    transform: scale(0.8);\n    transform-origin: 100% 50%;\n    opacity: 0;\n  }\n}\n@keyframes antZoomRightOut {\n  0% {\n    transform: scale(1);\n    transform-origin: 100% 50%;\n  }\n  100% {\n    transform: scale(0.8);\n    transform-origin: 100% 50%;\n    opacity: 0;\n  }\n}\n@-webkit-keyframes antZoomDownIn {\n  0% {\n    transform: scale(0.8);\n    transform-origin: 50% 100%;\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    transform-origin: 50% 100%;\n  }\n}\n@keyframes antZoomDownIn {\n  0% {\n    transform: scale(0.8);\n    transform-origin: 50% 100%;\n    opacity: 0;\n  }\n  100% {\n    transform: scale(1);\n    transform-origin: 50% 100%;\n  }\n}\n@-webkit-keyframes antZoomDownOut {\n  0% {\n    transform: scale(1);\n    transform-origin: 50% 100%;\n  }\n  100% {\n    transform: scale(0.8);\n    transform-origin: 50% 100%;\n    opacity: 0;\n  }\n}\n@keyframes antZoomDownOut {\n  0% {\n    transform: scale(1);\n    transform-origin: 50% 100%;\n  }\n  100% {\n    transform: scale(0.8);\n    transform-origin: 50% 100%;\n    opacity: 0;\n  }\n}\n.ant-motion-collapse-legacy {\n  overflow: hidden;\n}\n\n.ant-motion-collapse-legacy-active {\n  transition: height 0.15s cubic-bezier(0.645, 0.045, 0.355, 1), opacity 0.15s cubic-bezier(0.645, 0.045, 0.355, 1) !important;\n}\n\n.ant-motion-collapse {\n  overflow: hidden;\n  transition: height 0.15s cubic-bezier(0.645, 0.045, 0.355, 1), opacity 0.15s cubic-bezier(0.645, 0.045, 0.355, 1) !important;\n}\n\n.footer-contact {\n  font-size: 1.1vw;\n  padding-bottom: 1rem;\n}\n\n.footer-contact-mobile {\n  font-size: 4vw;\n  padding-bottom: 1rem;\n}\n\n.link-category {\n  color: #222222;\n  margin-left: 0;\n}\n\n.link-category-act {\n  border-bottom: 2px solid #2f80ed;\n  color: #2f80ed;\n  width: 65%;\n}\n\n.link-category:link {\n  color: #222222;\n}\n\n.link-category:visited {\n  border-bottom: 3px solid #2f80ed;\n  color: #2f80ed;\n}\n\n.link-category:hover {\n  border-bottom: 3px solid #2f80ed;\n  color: #2f80ed;\n  width: 65%;\n}\n\n.link-category:active {\n  border-bottom: 3px solid #2f80ed;\n  color: #2f80ed;\n  width: 65%;\n}\n\n.icon {\n  float: left;\n}\n\n.icon2 {\n  clear: both;\n}\n\n.icon-sosmed {\n  color: white;\n}\n\n.img1 {\n  background-repeat: no-repeat;\n  padding-bottom: 1.5rem;\n}\n\n.img2 {\n  background-repeat: no-repeat;\n  padding-bottom: 1.5rem;\n}\n\n.img3 {\n  background-repeat: no-repeat;\n  padding-bottom: 1.5rem;\n}\n\n.img4 {\n  padding-top: 1.25rem;\n  background-repeat: no-repeat;\n  padding-bottom: 1.75rem;\n}\n\n.ant-card-bordered {\n  border: 1px solid #f0f0f0;\n}\n\n.ant-card-bordered:hover {\n  border: 1px solid #f0f0f0;\n  box-shadow: 0px 0px 13px rgba(185, 185, 185, 0.685);\n}\n\n.productdetailsection {\n  background-color: #f5f5f5;\n  padding-top: 4.5rem;\n  padding-left: 9rem;\n  padding-right: 9rem;\n  padding-bottom: 13rem;\n}\n\n.insideleft {\n  float: left;\n  margin-bottom: 0.5rem;\n}\n\n.inside {\n  float: right;\n  margin-bottom: 0.5rem;\n}\n\n.menuBar {\n  padding: 0 20px;\n  border-bottom: solid 1px #e8e8e8;\n  overflow: auto;\n  box-shadow: 0 0 30px #f3f1f1;\n  background: linear-gradient(114deg, white 80.35%, #2f80ed 0%);\n}\n\n.ant-menu:not(.ant-menu-horizontal) .ant-menu-item-selected {\n  background-color: #f5f5f5;\n  border-bottom: 3px solid #2f80ed;\n  width: 65%;\n}\n\n.ant-menu-root.ant-menu-vertical,\n.ant-menu-root.ant-menu-vertical-left,\n.ant-menu-root.ant-menu-vertical-right,\n.ant-menu-root.ant-menu-inline {\n  box-shadow: none;\n  background-color: #f5f5f5;\n}\n\n.menuCon .ant-menu-item a,\n.menuCon .ant-menu-submenu-title a {\n  padding: 10px 20px !important;\n}\n\n.menu-antd {\n  background: linear-gradient(114deg, white 62.75%, #2f80ed 0%);\n  padding-top: 0.3rem;\n}\n\n.projectsection1 {\n  height: 50vh;\n  padding-top: 2.5rem;\n  background-image: url(\"/images/proyekheader.png\");\n  background-repeat: no-repeat;\n  background-size: contain;\n}\n\n.projectsection2 {\n  padding-top: 2.5rem;\n  padding-left: 3.5rem;\n  padding-right: 3.5rem;\n}\n\n.ant-menu-horizontal > .ant-menu-item,\n.ant-menu-horizontal > .ant-menu-submenu {\n  position: relative;\n  top: 1px;\n  display: inline-block;\n  vertical-align: bottom;\n  border: none !important;\n}\n\n.styling:link {\n  color: black;\n}\n\n.styling:visited {\n  color: #2f80ed;\n  font-weight: bold;\n}\n\n.styling:hover {\n  color: #2f80ed;\n  border-bottom: 3px solid #1890ff;\n}\n\n.container-icon {\n  padding-top: 100%;\n  padding-bottom: 1rem;\n}\n\n.icon-sosmed {\n  font-size: 5vw;\n  color: white;\n  padding: 1.5rem;\n}\n\n.aboutsection1 {\n  height: 50vh;\n  background-image: url(\"/image/section2.svg\");\n  background-repeat: no-repeat;\n  background-size: cover;\n  padding-top: 4rem;\n}\n\n.aboutsection1-mobile {\n  background-color: #f5f5f5;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  padding-top: 6rem;\n}\n\n.blogsection1-mobile {\n  background-color: #f5f5f5;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  padding-top: 3rem;\n  padding-bottom: 4rem;\n}\n\n.aboutsection2 {\n  padding: 3.5rem;\n  background-color: #f5f5f5;\n}\n\n.blogsection {\n  background-color: #f5f5f5;\n}\n\n.aboutsection3 {\n  padding: 3.5rem;\n  background-color: #2f80ed;\n}\n\n.aboutsection4 {\n  padding: 3.5rem 3.5rem 13rem 0;\n}\n\n.ant-input-suffix {\n  margin-left: 4px;\n  width: 2vw;\n}\n\n.section1 {\n  height: 90vh;\n  background-image: url(\"/image/background-main.png\");\n  background-repeat: no-repeat;\n  background-size: cover;\n  padding-top: 2rem;\n}\n\n.section1-mobile {\n  background-image: url(/image/mobile-main.png);\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 60vh;\n  padding: 4rem 1.5rem 1rem 1.5rem;\n}\n\n.section2 {\n  height: 55vh;\n  background-image: url(\"/image/section2.svg\");\n  background-repeat: no-repeat;\n  background-size: cover;\n  padding-top: 4rem;\n}\n\n.section2-mobile {\n  background-image: url(\"/image/timeline-mobilee.svg\");\n  background-repeat: no-repeat;\n  background-size: cover;\n  padding: 2rem 1.5rem;\n}\n\n.section3 {\n  background-color: #f5f5f5;\n  padding-left: 3.5rem;\n  padding-right: 3.5rem;\n  padding-top: 5rem;\n}\n\n.productsection1 {\n  background-color: #f5f5f5;\n  padding-left: 9rem;\n  padding-right: 9rem;\n  padding-top: 7rem;\n  padding-bottom: 5rem;\n}\n\n.productsection1-mobile {\n  background-color: #f5f5f5;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  padding-top: 6rem;\n  padding-bottom: 6rem;\n}\n\n.customersection1-mobile {\n  background-color: #f5f5f5;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  padding-bottom: 6rem;\n}\n\n.sectionbutton {\n  padding-left: 3.5rem;\n  padding-right: 3.5rem;\n  padding-top: 11.3rem;\n}\n\n.sectionbuttonproject {\n  padding-bottom: 3.5rem;\n}\n\n.section3-mobile {\n  padding-left: 1.5rem;\n  padding-right: 1.5rem;\n  padding-top: 3rem;\n  background-color: #f5f5f5;\n}\n\n.section4 {\n  background-color: #f5f5f5;\n  height: 60vh;\n  padding-left: 3.5rem;\n  padding-right: 3.5rem;\n  padding-top: 6rem;\n  padding-bottom: 37rem;\n}\n\n.section4-mobile {\n  background-color: #f5f5f5;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  padding-top: 1.5rem;\n  padding-bottom: 3rem;\n}\n\n.section9 {\n  padding-left: 3.5rem;\n  padding-right: 3.5rem;\n  padding-top: 2rem;\n  background-color: #f5f5f5;\n}\n\n.section9-mobile {\n  padding-left: 2rem;\n  padding-right: 2rem;\n  padding-top: 2rem;\n  background-color: #f5f5f5;\n}\n\n.ant-drawer-body {\n  padding: 0;\n  background: #2f80ed;\n}\n\n.menuBar {\n  padding: 0 20px;\n  border-bottom: solid 1px #e8e8e8;\n  overflow: auto;\n  box-shadow: 0 0 30px #f3f1f1;\n}\n\n.ant-card-body {\n  padding: 0px;\n}\n\n.ant-modal-body {\n  padding: 4rem;\n  font-size: 14px;\n  line-height: 1.5715;\n  word-wrap: break-word;\n}\n\n.logo2 {\n  float: left;\n  padding-top: 3.5rem;\n}\n\n.logo {\n  width: 150px;\n  float: left;\n}\n\n.logo a {\n  display: inline-block;\n  font-size: 20px;\n  padding: 10px 20px 4px;\n  border: none;\n  text-transform: capitalize;\n}\n\n.textinfo {\n  float: left;\n  padding-right: 0.5rem;\n}\n\n.textinfodesc {\n  padding-right: 1.2rem;\n  float: left;\n  font-size: 0.9vw;\n}\n\n.title-invest {\n  font-weight: bold;\n  font-size: 1.4vw;\n  padding: 1rem 1rem 0rem 1rem;\n  text-align: left;\n}\n\n.textinfoprogress {\n  clear: both;\n}\n\n.padding-invest {\n  padding: 1rem;\n  text-align: left;\n}\n\n.section4-left {\n  padding-right: 0rem;\n}\n\n.blog {\n  float: left;\n  padding-right: 1rem;\n}\n\n.product-list {\n  padding: 1rem;\n  text-align: center;\n}\n\n.product-list-mobile {\n  padding: 1rem;\n  text-align: left;\n}\n\n.float {\n  float: left;\n}\n\n.ant-divider-horizontal {\n  display: block;\n  clear: both;\n  width: 100%;\n  min-width: 100%;\n  height: 1px;\n  margin: 10px 0;\n}\n\n.barsMenu {\n  float: right;\n  height: 32px;\n  padding: 0px 6px 6px 6px !important;\n  margin-top: 0px !important;\n  display: none;\n  border: none;\n  background: white !important;\n  box-shadow: 0 0px 0 rgba(0, 0, 0, 0.045);\n}\n\n.row-navbar {\n  position: fixed;\n  background-color: white;\n  width: 100%;\n  z-index: 5;\n  box-shadow: 0 0 30px #d3d3d377;\n}\n\n.row-navbar-mobile {\n  position: fixed;\n  background-color: white;\n  width: 100%;\n  z-index: 5;\n  box-shadow: 0 0 30px #d3d3d377;\n  padding-left: 1rem;\n  padding-right: 1rem;\n}\n\n.menuCon {\n  width: calc(100% - 150px);\n  float: right;\n}\n\n.menuCon .ant-menu-item {\n  padding: 0px 5px;\n}\n\n.menuCon .ant-menu-submenu-title {\n  padding: 10px 20px;\n}\n\n.menuCon .ant-menu-item a,\n.menuCon .ant-menu-submenu-title a {\n  padding: 10px 15px;\n}\n\n.menuCon .ant-menu-horizontal {\n  border-bottom: none;\n}\n\n.menuCon .leftMenu {\n  float: left;\n}\n\n.menuCon .rightMenu {\n  float: right;\n}\n\n.search {\n  width: 100%;\n  height: 8vh;\n  padding: 0.9rem;\n}\n\n.barsBtn {\n  display: block;\n  width: 20px;\n  height: 2px;\n  background: #2f80ed;\n  position: relative;\n}\n\n.ant-btn-primary {\n  background: #2f80ed;\n}\n\n.ant-divider-horizontal {\n  display: block;\n  clear: both;\n  width: 40%;\n  min-width: 40%;\n  height: 5px;\n  margin: 10px 0;\n  background-color: #2f80ed;\n}\n\n.barsBtn:after,\n.barsBtn:before {\n  content: attr(x);\n  width: 20px;\n  position: absolute;\n  top: -6px;\n  left: 0;\n  height: 2px;\n  background: #2f80ed;\n}\n\n.barsBtn:after {\n  top: auto;\n  bottom: -6px;\n}\n\n.ant-drawer-body {\n  padding: 0;\n}\n\n.ant-drawer-body .ant-menu-horizontal > .ant-menu-item,\n.ant-drawer-body .ant-menu-horizontal > .ant-menu-submenu {\n  display: inline-block;\n  width: 100%;\n}\n\n.ant-drawer-body .ant-menu-horizontal {\n  border-bottom: none;\n}\n\n.ant-drawer-body .ant-menu-horizontal > .ant-menu-item:hover {\n  border-bottom-color: transparent;\n}\n\n@media (max-width: 360px) {\n  .section4-right {\n    padding-right: 0.5rem;\n  }\n\n  .section4-left {\n    padding-left: 0.5rem;\n  }\n\n  .section4-mobile {\n    background-color: #f5f5f5;\n    padding-left: 0rem;\n    padding-right: 0rem;\n    padding-top: 1.5rem;\n    padding-bottom: 3rem;\n  }\n\n  .container-icon {\n    padding-top: 70%;\n  }\n}\n@media (max-width: 767px) {\n  .barsMenu {\n    display: inline-block;\n  }\n\n  .leftMenu,\n.rightMenu {\n    display: none;\n  }\n\n  .logo a {\n    margin-left: -20px;\n  }\n\n  .menuCon .ant-menu-item,\n.menuCon .ant-menu-submenu-title {\n    padding: 1px 20px;\n  }\n\n  .logo a {\n    padding: 10px 20px;\n  }\n\n  .topinfo {\n    font-size: 20px;\n  }\n\n  .ant-menu-root.ant-menu-vertical,\n.ant-menu-root.ant-menu-vertical-left,\n.ant-menu-root.ant-menu-vertical-right,\n.ant-menu-root.ant-menu-inline {\n    box-shadow: none;\n    background: #2f80ed;\n  }\n}\n.ant-menu-horizontal > .ant-menu-item-selected > a {\n  border-bottom: 3px solid #1890ff;\n}\n\n.top {\n  padding-top: 3rem;\n  padding-bottom: 3rem;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./resources/js/App.scss":
/*!*******************************!*\
  !*** ./resources/js/App.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader!../../node_modules/postcss-loader/src??ref--7-2!../../node_modules/sass-loader/dist/cjs.js??ref--7-3!./App.scss */ "./node_modules/css-loader/index.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./resources/js/App.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./resources/js/ScrollToTop.js":
/*!*************************************!*\
  !*** ./resources/js/ScrollToTop.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");



function ScrollToTop(_ref) {
  var history = _ref.history;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var unlisten = history.listen(function () {
      window.scrollTo(0, 0);
    });
    return function () {
      unlisten();
    };
  }, []);
  return null;
}

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["withRouter"])(ScrollToTop));

/***/ }),

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index */ "./resources/js/index.js");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
/* harmony import */ var _store_newsStore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./store/newsStore */ "./resources/js/store/newsStore.js");





var stores = {
  newsStore: _store_newsStore__WEBPACK_IMPORTED_MODULE_4__["default"]
};
react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(mobx_react__WEBPACK_IMPORTED_MODULE_3__["Provider"], stores, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_index__WEBPACK_IMPORTED_MODULE_2__["default"], null)), document.getElementById("app"));

/***/ }),

/***/ "./resources/js/components/Beranda/Section1.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/Beranda/Section1.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var Search = antd__WEBPACK_IMPORTED_MODULE_1__["Input"].Search;
var Section1 = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_4__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_4__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(Section1, _Component);

  function Section1() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Section1);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Section1)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth,
      visible: false,
      confirmLoading: false,
      searchText: ""
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_3___default.a.init({
        duration: 1000,
        delay: 50
      });
    };

    _this.showModal = function () {
      _this.setState({
        visible: true
      });
    };

    _this.handleOk = function () {
      _this.setState({
        confirmLoading: true
      });

      setTimeout(function () {
        _this.setState({
          visible: false,
          confirmLoading: false
        });
      }, 2000);
    };

    _this.handleCancel = function () {
      _this.setState({
        visible: false
      });
    };

    _this.handleSearch = function (e) {
      e.preventDefault();
      var data = {
        q: _this.state.searchText
      };

      _this.props.newsStore.search(data);

      _this.props.history.push("/search");
    };

    return _this;
  }

  _createClass(Section1, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        className: "section1",
        type: "flex",
        justify: "center",
        align: "middle"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        xs: 24,
        md: 24,
        style: {
          textAlign: "center"
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
        onSubmit: this.handleSearch
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Search, {
        placeholder: "Cari Produk",
        style: {
          width: "80%",
          height: "8vh",
          padding: "0.9rem"
        },
        onChange: function onChange(e) {
          return _this2.setState({
            searchText: e.target.value
          });
        }
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        align: "middle",
        className: "top"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 6,
        align: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/star-icon.svg")
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          fontSize: "1.1vw"
        }
      }, "Kualitas Terbaik")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 6,
        align: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/guarentee.svg")
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          fontSize: "1.1vw"
        }
      }, "Bergaransi")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 6,
        align: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/lower-price.svg")
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          fontSize: "1.1vw"
        }
      }, "Harga Terjangkau")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 6,
        align: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/expedition.svg")
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          fontSize: "1.1vw"
        }
      }, "Gratis Pengiriman"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        style: {
          backgroundColor: "#f5f5f5",
          paddingTop: "7rem",
          paddingBottom: "7rem",
          paddingRight: "10rem",
          paddingLeft: "10rem"
        },
        justify: "center",
        align: "middle"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 8,
        align: "left",
        justify: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/logo-printugo.svg")
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 16,
        align: "left"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          fontSize: "1.1vw",
          color: "black"
        }
      }, "Berdiri sejak tahun 2016 sebagai Startup yang fokus pada Periklanan Transportasi, Sticar Group\xA0berekspansi ke bisnis periklanan dan percetakan luar ruang (Outdoor Advertising) yang lebih luas dan mendirikan Printugo.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "Kami menyediakan platform jasa percetakan luar ruang untuk bisnis anda dengan kualitas baik dan harga terjangkau"))));
    }
  }]);

  return Section1;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["withRouter"])(Section1));

/***/ }),

/***/ "./resources/js/components/Beranda/Section2.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/Beranda/Section2.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




var Title = antd__WEBPACK_IMPORTED_MODULE_1__["Typography"].Title;

var Section2 = /*#__PURE__*/function (_Component) {
  _inherits(Section2, _Component);

  function Section2() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Section2);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Section2)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth,
      visible: false,
      confirmLoading: false
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_2___default.a.init({
        duration: 1000,
        delay: 50
      });
    };

    _this.showModal = function () {
      _this.setState({
        visible: true
      });
    };

    _this.handleOk = function () {
      _this.setState({
        confirmLoading: true
      });

      setTimeout(function () {
        _this.setState({
          visible: false,
          confirmLoading: false
        });
      }, 2000);
    };

    _this.handleCancel = function () {
      _this.setState({
        visible: false
      });
    };

    return _this;
  }

  _createClass(Section2, [{
    key: "render",
    value: function render() {
      var _this$state = this.state,
          visible = _this$state.visible,
          confirmLoading = _this$state.confirmLoading,
          width = _this$state.width;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        className: "section2",
        justify: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        "data-aos": "slide-right",
        "data-aos-delay": "550",
        xs: 24,
        align: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.pathname, "image/timeline.png"),
        className: "img1"
      }))));
    }
  }]);

  return Section2;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Section2);

/***/ }),

/***/ "./resources/js/components/Beranda/Section3.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/Beranda/Section3.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var Title = antd__WEBPACK_IMPORTED_MODULE_1__["Typography"].Title;
var Section3 = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(Section3, _Component);

  function Section3() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Section3);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Section3)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth,
      visible: false,
      confirmLoading: false
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_2___default.a.init({
        duration: 1000,
        delay: 50
      });
    };

    _this.showModal = function () {
      _this.setState({
        visible: true
      });
    };

    _this.handleOk = function () {
      _this.setState({
        confirmLoading: true
      });

      setTimeout(function () {
        _this.setState({
          visible: false,
          confirmLoading: false
        });
      }, 2000);
    };

    _this.handleCancel = function () {
      _this.setState({
        visible: false
      });
    };

    return _this;
  }

  _createClass(Section3, [{
    key: "render",
    value: function render() {
      var dataProduct = this.props.newsStore.dataProduct;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        className: "section3",
        justify: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        "data-aos": "slide-right",
        "data-aos-delay": "550",
        xs: 24,
        align: "left"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Title, {
        level: 4,
        className: "text-white",
        style: {
          fontSize: "1.9vw",
          fontWeight: "bold"
        }
      }, "Our Product")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        type: "flex",
        gutter: 48,
        justify: "space-around",
        "data-aos": "slide-right"
      }, dataProduct.map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 8,
          align: "center",
          style: {
            paddingBottom: "2.5rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_4__["Link"], {
          to: "/productdetail/".concat(item.id)
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Card"], {
          style: {
            width: "27vw"
          },
          cover: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            src: "".concat(window.location.origin, "/storage/").concat(item.image),
            style: {
              width: "100%",
              height: "auto"
            }
          })
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          className: "title-invest"
        }, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "padding-invest"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, "Kami menyediakan jasa periklanan di transportasi mobil bagi anda yang ingin mengenalkan brand anda."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          style: {
            paddingTop: "1.5rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 2,
          className: "float"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/label.svg")
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 22
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "1.5vw",
            marginLeft: "0.7rem",
            fontWeight: "bold",
            color: "#2F80ED"
          }
        }, item.harga)))))));
      }))));
    }
  }]);

  return Section3;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (Section3);

/***/ }),

/***/ "./resources/js/components/Beranda/Section4.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/Beranda/Section4.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var Section4 = /*#__PURE__*/function (_Component) {
  _inherits(Section4, _Component);

  function Section4() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Section4);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Section4)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth,
      visible: false,
      confirmLoading: false
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_2___default.a.init({
        duration: 1000,
        delay: 50
      });
    };

    _this.showModal = function () {
      _this.setState({
        visible: true
      });
    };

    _this.handleOk = function () {
      _this.setState({
        confirmLoading: true
      });

      setTimeout(function () {
        _this.setState({
          visible: false,
          confirmLoading: false
        });
      }, 2000);
    };

    _this.handleCancel = function () {
      _this.setState({
        visible: false
      });
    };

    return _this;
  }

  _createClass(Section4, [{
    key: "render",
    value: function render() {
      var _this$state = this.state,
          visible = _this$state.visible,
          confirmLoading = _this$state.confirmLoading,
          width = _this$state.width;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        className: "section4",
        justify: "center",
        align: "middle"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        "data-aos": "slide-right",
        "data-aos-delay": "550",
        md: 5,
        align: "left",
        style: {
          marginBottom: 30
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          fontSize: "1.5vw",
          fontWeight: "bold"
        }
      }, "Apa Kata Mereka")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 19,
        "data-aos": "slide-left",
        "data-aos-delay": "550"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        align: "middle"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 8
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/picture-testimonial.png")
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 16
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          fontSize: "1.5vw"
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        style: {
          color: "#2F80ED"
        }
      }, "Affan Awangga"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        style: {
          fontWeight: "bold"
        }
      }, " ", "- OYO Rooms")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          fontSize: "1.5vw",
          paddingTop: "4rem",
          paddingBottom: "4rem"
        }
      }, "\"Kalau soal kecepatan dan kualitas Sticar nomor satu. Hasilnya juga memuaskan. Kita pesan Signage untuk OYO di 100 titik dan selesai tempat waktu.\""), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/oyo-logo.svg")
      }))))));
    }
  }]);

  return Section4;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Section4);

/***/ }),

/***/ "./resources/js/components/Footer.js":
/*!*******************************************!*\
  !*** ./resources/js/components/Footer.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var Footer = /*#__PURE__*/function (_Component) {
  _inherits(Footer, _Component);

  function Footer() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Footer);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Footer)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_2___default.a.init({
        duration: 1000,
        delay: 50
      });
    };

    return _this;
  }

  _createClass(Footer, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        className: "section9",
        justify: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 6,
        align: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/printugo-footer.svg")
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 10
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "icon"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/location.svg")
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        className: "footer-contact"
      }, "Printugo", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "Jl. Kemang Timur No 12, Mampang Prapatan, Jakarta Selatan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "icon"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/email.svg")
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        className: "footer-contact"
      }, "hello@printugo.co.id"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "icon"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/phone.svg")
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        className: "footer-contact"
      }, "+62 822 2132 3726")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 4
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
        to: "/product"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          marginBottom: "0.8rem"
        }
      }, "Product")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
        to: "/blog"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          marginBottom: "0.8rem"
        }
      }, "Blog"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        md: 4
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
        to: "ourcustomer"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          marginBottom: "0.8rem"
        }
      }, "Portofolio")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["Link"], {
        to: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        style: {
          marginBottom: "0.8rem"
        }
      }, "About Us"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Divider"], {
        style: {
          height: "2px",
          width: "100%",
          background: "#afafaf"
        }
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, " Copyright @ 2020 Printugo")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        justify: "center",
        style: {
          paddingBottom: 10
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, "PT. Optimal Digital Solusi")));
    }
  }]);

  return Footer;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./resources/js/components/FooterMobile.js":
/*!*************************************************!*\
  !*** ./resources/js/components/FooterMobile.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var FooterMobile = /*#__PURE__*/function (_Component) {
  _inherits(FooterMobile, _Component);

  function FooterMobile() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FooterMobile);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FooterMobile)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_2___default.a.init({
        duration: 1000,
        delay: 50
      });
    };

    return _this;
  }

  _createClass(FooterMobile, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        className: "section9-mobile",
        justify: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        xs: 24,
        align: "center"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/footer-logo.svg")
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        xs: 24,
        style: {
          paddingTop: "2rem"
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        xs: 3
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "icon"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/location.svg")
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        xs: 21
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        className: "footer-contact-mobile"
      }, "Printugo", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "Jl. Kemang Timur No 12, Mampang Prapatan, Jakarta Selatan")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        xs: 3
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "icon"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/email.svg")
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        xs: 21
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        className: "footer-contact-mobile"
      }, "hello@printugo.co.id")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        xs: 3
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "icon"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/phone.svg")
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
        xs: 21
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
        className: "footer-contact-mobile"
      }, "+62 822 2132 3726")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Divider"], {
        style: {
          height: "2px",
          width: "100%",
          background: "#afafaf"
        }
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, " Copyright @ 2020 Printugo")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
        justify: "center",
        style: {
          paddingBottom: 10
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, "PT. Optimal Digital Solusi")));
    }
  }]);

  return FooterMobile;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (FooterMobile);

/***/ }),

/***/ "./resources/js/components/Navbar.js":
/*!*******************************************!*\
  !*** ./resources/js/components/Navbar.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _RightMenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RightMenu */ "./resources/js/components/RightMenu.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var Navbar = /*#__PURE__*/function (_Component) {
  _inherits(Navbar, _Component);

  function Navbar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Navbar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Navbar)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      current: "mail",
      visible: false
    };

    _this.showDrawer = function () {
      _this.setState({
        visible: true
      });
    };

    _this.onClose = function () {
      _this.setState({
        visible: false
      });
    };

    return _this;
  }

  _createClass(Navbar, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
        align: "middle",
        className: "row-navbar"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        md: 12
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "logo"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: ""
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/printugo-logo.svg")
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        md: 12
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "menuCon"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "rightMenu"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RightMenu__WEBPACK_IMPORTED_MODULE_1__["default"], null)))));
    }
  }]);

  return Navbar;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Navbar);

/***/ }),

/***/ "./resources/js/components/NavbarMobile.js":
/*!*************************************************!*\
  !*** ./resources/js/components/NavbarMobile.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _RightMenuMobile__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RightMenuMobile */ "./resources/js/components/RightMenuMobile.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var NavbarMobile = /*#__PURE__*/function (_Component) {
  _inherits(NavbarMobile, _Component);

  function NavbarMobile() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, NavbarMobile);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(NavbarMobile)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      current: "sub1",
      visible: false
    };

    _this.showDrawer = function () {
      _this.setState({
        visible: true
      });
    };

    _this.onClose = function () {
      _this.setState({
        visible: false
      });
    };

    return _this;
  }

  _createClass(NavbarMobile, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
        align: "middle",
        className: "row-navbar-mobile"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        xs: 20
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "logo"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/logo-navbar.svg")
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        xs: 4
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "menuCon"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        className: "barsMenu",
        type: "primary",
        onClick: this.showDrawer
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "barsBtn"
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Drawer"], {
        style: {
          color: "#23ad46"
        },
        placement: "right",
        closable: false,
        onClose: this.onClose,
        visible: this.state.visible
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RightMenuMobile__WEBPACK_IMPORTED_MODULE_1__["default"], null)))));
    }
  }]);

  return NavbarMobile;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (NavbarMobile);

/***/ }),

/***/ "./resources/js/components/RightMenu.js":
/*!**********************************************!*\
  !*** ./resources/js/components/RightMenu.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _ant_design_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ant-design/icons */ "./node_modules/@ant-design/icons/es/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var RightMenu = /*#__PURE__*/function (_Component) {
  _inherits(RightMenu, _Component);

  function RightMenu() {
    _classCallCheck(this, RightMenu);

    return _possibleConstructorReturn(this, _getPrototypeOf(RightMenu).apply(this, arguments));
  }

  _createClass(RightMenu, [{
    key: "render",
    value: function render() {
      var href = window.location.href.split("/");
      href = href[4];
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"], {
        className: "menu-antd",
        mode: "horizontal",
        defaultSelectedKeys: ["/" + href],
        selectedKeys: ["/" + href]
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "/"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: "/",
        className: "styling"
      }, "Home")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "/product"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: "/product",
        className: "styling"
      }, "Product")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "/ourcustomer"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: "/ourcustomer",
        className: "styling"
      }, "Our Customer")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "/blog"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: "/blog",
        className: "styling"
      }, "Blog")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "help"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://www.linkedin.com/company/printugo",
        target: "_blank"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_3__["LinkedinFilled"], {
        style: {
          fontSize: "1.4vw",
          color: "white"
        }
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "sign"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://www.instagram.com/printugo/",
        target: "_blank"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_3__["InstagramFilled"], {
        style: {
          fontSize: "1.4vw",
          color: "white"
        }
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "login"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://www.facebook.com/Printugo-108425620748937/",
        target: "_blank"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_3__["FacebookFilled"], {
        style: {
          fontSize: "1.4vw",
          color: "white"
        }
      }))));
    }
  }]);

  return RightMenu;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (RightMenu);

/***/ }),

/***/ "./resources/js/components/RightMenuMobile.js":
/*!****************************************************!*\
  !*** ./resources/js/components/RightMenuMobile.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _ant_design_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ant-design/icons */ "./node_modules/@ant-design/icons/es/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var RightMenuMobile = /*#__PURE__*/function (_Component) {
  _inherits(RightMenuMobile, _Component);

  function RightMenuMobile() {
    _classCallCheck(this, RightMenuMobile);

    return _possibleConstructorReturn(this, _getPrototypeOf(RightMenuMobile).apply(this, arguments));
  }

  _createClass(RightMenuMobile, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        style: {
          backgroundColor: "white",
          padding: "0.2rem",
          fontSize: "4vw",
          textAlign: "center"
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
        src: "".concat(window.location.origin, "/image/logo-mobile.svg"),
        style: {
          width: "20vw"
        }
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"], {
        style: {
          paddingTop: "1rem",
          paddingBottom: "50%"
        },
        defaultOpenKeys: ["sub1"],
        mode: "inline"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "sub1"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: "/",
        style: {
          color: "white",
          fontSize: "4.5vw"
        }
      }, "Home")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "sub2"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: "/product",
        style: {
          color: "white",
          fontSize: "4.5vw"
        }
      }, "Product")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "sub3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: "/ourcustomer",
        style: {
          color: "white",
          fontSize: "4.5vw"
        }
      }, "Our Customer")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "sub4"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: "/blog",
        style: {
          color: "white",
          fontSize: "4.5vw"
        }
      }, "Blog"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        align: "center",
        className: "container-icon"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://www.linkedin.com/company/printugo",
        target: "_blank"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_3__["LinkedinFilled"], {
        className: "icon-sosmed"
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://www.instagram.com/printugo/",
        target: "_blank"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_3__["InstagramFilled"], {
        className: "icon-sosmed"
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "https://www.facebook.com/Printugo-108425620748937/",
        target: "_blank"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_3__["FacebookFilled"], {
        className: "icon-sosmed"
      }))));
    }
  }]);

  return RightMenuMobile;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (RightMenuMobile);

/***/ }),

/***/ "./resources/js/index.js":
/*!*******************************!*\
  !*** ./resources/js/index.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _pages_Home__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages/Home */ "./resources/js/pages/Home.js");
/* harmony import */ var _pages_Product__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/Product */ "./resources/js/pages/Product.js");
/* harmony import */ var _pages_OurCustomer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/OurCustomer */ "./resources/js/pages/OurCustomer.js");
/* harmony import */ var _pages_OurCustomerDesc__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/OurCustomerDesc */ "./resources/js/pages/OurCustomerDesc.js");
/* harmony import */ var _pages_Blog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/Blog */ "./resources/js/pages/Blog.js");
/* harmony import */ var _pages_BlogDesc__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/BlogDesc */ "./resources/js/pages/BlogDesc.js");
/* harmony import */ var _pages_ProductDetail__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/ProductDetail */ "./resources/js/pages/ProductDetail.js");
/* harmony import */ var _ScrollToTop__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ScrollToTop */ "./resources/js/ScrollToTop.js");
/* harmony import */ var _pages_SearchResult__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/SearchResult */ "./resources/js/pages/SearchResult.js");
/* harmony import */ var aos_dist_aos_css__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! aos/dist/aos.css */ "./node_modules/aos/dist/aos.css");
/* harmony import */ var aos_dist_aos_css__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(aos_dist_aos_css__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var antd_dist_antd_less__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! antd/dist/antd.less */ "./node_modules/antd/dist/antd.less");
/* harmony import */ var antd_dist_antd_less__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(antd_dist_antd_less__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _App_scss__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./App.scss */ "./resources/js/App.scss");
/* harmony import */ var _App_scss__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_App_scss__WEBPACK_IMPORTED_MODULE_13__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
















var App = /*#__PURE__*/function (_Component) {
  _inherits(App, _Component);

  function App() {
    _classCallCheck(this, App);

    return _possibleConstructorReturn(this, _getPrototypeOf(App).apply(this, arguments));
  }

  _createClass(App, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["HashRouter"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ScrollToTop__WEBPACK_IMPORTED_MODULE_9__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/",
        component: _pages_Home__WEBPACK_IMPORTED_MODULE_2__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/product",
        component: _pages_Product__WEBPACK_IMPORTED_MODULE_3__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/ourcustomer",
        component: _pages_OurCustomer__WEBPACK_IMPORTED_MODULE_4__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/ourcustomerdesc",
        component: _pages_OurCustomerDesc__WEBPACK_IMPORTED_MODULE_5__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/blog",
        component: _pages_Blog__WEBPACK_IMPORTED_MODULE_6__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/blogdesc/:id",
        component: _pages_BlogDesc__WEBPACK_IMPORTED_MODULE_7__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/productdetail/:id",
        component: _pages_ProductDetail__WEBPACK_IMPORTED_MODULE_8__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/search",
        component: _pages_SearchResult__WEBPACK_IMPORTED_MODULE_10__["default"]
      })));
    }
  }]);

  return App;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./resources/js/pages/Blog.js":
/*!************************************!*\
  !*** ./resources/js/pages/Blog.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Navbar */ "./resources/js/components/Navbar.js");
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Footer */ "./resources/js/components/Footer.js");
/* harmony import */ var _components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/FooterMobile */ "./resources/js/components/FooterMobile.js");
/* harmony import */ var _components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/NavbarMobile */ "./resources/js/components/NavbarMobile.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_8__);
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }










var Home = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_7__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_7__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(Home, _Component);

  function Home() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Home);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Home)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth,
      currentPage: 1,
      todosPerPage: 3
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_8___default.a.init({
        duration: 1000,
        delay: 50
      });

      _this.props.newsStore.news();
    };

    _this.handleWindowSizeChange = function () {
      _this.setState({
        width: window.innerWidth
      });
    };

    return _this;
  }

  _createClass(Home, [{
    key: "UNSAFE_componentWillMount",
    value: function UNSAFE_componentWillMount() {
      window.addEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          width = _this$state.width,
          currentPage = _this$state.currentPage,
          todosPerPage = _this$state.todosPerPage;
      var isMobile = width <= 500;
      var dataNews = this.props.newsStore.dataNews;
      var indexOfLastTodo = currentPage * todosPerPage;
      var indexOfFirstTodo = indexOfLastTodo - todosPerPage;
      var currentTodos = dataNews.slice(indexOfFirstTodo, indexOfLastTodo);
      var pageNumbers = [];

      for (var i = 1; i <= Math.ceil(dataNews.length / todosPerPage); i++) {
        pageNumbers.push(i);
      }

      var renderPageNumbers = pageNumbers.map(function (number) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: number,
          id: number,
          onClick: _this2.handleClick
        }, number);
      }); //untuk pc//

      if (!isMobile) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Navbar__WEBPACK_IMPORTED_MODULE_2__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "aboutsection1",
          type: "flex",
          justify: "space-around",
          align: "middle"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          md: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          "data-aos": "zoom-in-up",
          level: 1,
          style: {
            fontSize: "2.7vw",
            color: "white",
            textAlign: "center"
          }
        }, "Blog dan Media"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "aboutsection2",
          align: "middle",
          justify: "space-around"
        }, currentTodos.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            md: 8,
            style: {
              padding: "0.5rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
            to: "/blogdesc/".concat(item.id)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            src: "".concat(window.location.origin, "/storage/").concat(item.image),
            style: {
              width: "100%",
              paddingBottom: "1rem"
            }
          }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "1.5vw"
            }
          }, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, item.resume)));
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          id: "page-numbers"
        }, renderPageNumbers))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Footer__WEBPACK_IMPORTED_MODULE_3__["default"], null));
      } else {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          style: {
            paddingLeft: "1rem",
            paddingRight: "1rem",
            paddingTop: "6rem",
            paddingBottom: "6rem",
            backgroundColor: "#f5f5f5"
          },
          type: "flex",
          justify: "space-around",
          gutter: 20
        }, currentTodos.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            xs: 24,
            md: 24
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
            to: "/blogdesc/".concat(item.id)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "blog"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            src: "".concat(window.location.origin, "/storage/").concat(item.image),
            style: {
              width: "40vw",
              height: "15vh",
              paddingBottom: "1rem"
            }
          })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "blogdesc"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "4.5vw",
              fontWeight: "bold"
            }
          }, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "2.5vw",
              paddingTop: "0.5rem"
            }
          }, item.resume))));
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__["default"], null));
      }
    }
  }]);

  return Home;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ "./resources/js/pages/BlogDesc.js":
/*!****************************************!*\
  !*** ./resources/js/pages/BlogDesc.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Navbar */ "./resources/js/components/Navbar.js");
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Footer */ "./resources/js/components/Footer.js");
/* harmony import */ var _components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/FooterMobile */ "./resources/js/components/FooterMobile.js");
/* harmony import */ var _components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/NavbarMobile */ "./resources/js/components/NavbarMobile.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_8__);
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }










var BlogDesc = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_7__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_7__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(BlogDesc, _Component);

  function BlogDesc() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, BlogDesc);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(BlogDesc)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth
    };

    _this.componentDidMount = function () {
      var idUrl = window.location.hash;
      var data = {
        id: idUrl.replace("#/blogdesc/", "")
      };
      aos__WEBPACK_IMPORTED_MODULE_8___default.a.init({
        duration: 1000,
        delay: 50
      });

      _this.props.newsStore.oneblog(data);

      _this.props.newsStore.news();
    };

    _this.componentDidUpdate = function () {
      var idUrl = window.location.hash;
      var data = {
        id: idUrl.replace("#/blogdesc/", "")
      };

      _this.props.newsStore.oneblog(data);
    };

    _this.handleWindowSizeChange = function () {
      _this.setState({
        width: window.innerWidth
      });
    };

    return _this;
  }

  _createClass(BlogDesc, [{
    key: "UNSAFE_componentWillMount",
    value: function UNSAFE_componentWillMount() {
      window.addEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "render",
    value: function render() {
      var width = this.state.width;
      var isMobile = width <= 500;
      var _this$props$newsStore = this.props.newsStore,
          dataOneBlog = _this$props$newsStore.dataOneBlog,
          dataNews = _this$props$newsStore.dataNews; //untuk pc//

      if (!isMobile) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Navbar__WEBPACK_IMPORTED_MODULE_2__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "blogsection",
          justify: "space-around"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/storage/").concat(dataOneBlog.image),
          style: {
            width: "100vw",
            paddingBottom: "1rem"
          }
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginTop: "-5rem",
            fontSize: "2.3vw",
            fontWeight: "bold",
            paddingRight: "4rem",
            paddingLeft: "4rem",
            color: "white"
          }
        }, dataOneBlog.resume)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 6,
          style: {
            padding: "4rem 2rem 1.5rem 4rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "1.2vw"
          }
        }, dataOneBlog.title)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 18,
          style: {
            padding: "4rem 18rem 4.5rem 1rem",
            textAlign: "justify"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          dangerouslySetInnerHTML: {
            __html: dataOneBlog.desc
          }
        })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "aboutsection2",
          align: "middle",
          justify: "space-around"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "1.6vw",
            fontWeight: "bold",
            paddingBottom: "2rem"
          }
        }, "Artikel Lainnya")), dataNews.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            md: 6,
            style: {
              padding: "0.5rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
            to: "/blogdesc/".concat(item.id)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            src: "".concat(window.location.origin, "/storage/").concat(item.image),
            style: {
              width: "100%",
              paddingBottom: "1rem"
            }
          }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "1.5vw"
            }
          }, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, item.resume)));
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Footer__WEBPACK_IMPORTED_MODULE_3__["default"], null));
      } else {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "blogsection1-mobile"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          align: "left",
          style: {
            paddingBottom: "1rem",
            paddingTop: "1.5rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          to: "/blog"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/back.svg")
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/storage/").concat(dataOneBlog.image),
          style: {
            width: "100%",
            paddingBottom: "1rem"
          }
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "4vw",
            fontWeight: "bold",
            color: "#2f80ed"
          }
        }, dataOneBlog.resume)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3vw",
            paddingTop: "1rem",
            paddingBottom: "1rem"
          }
        }, dataOneBlog.title)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          style: {
            textAlign: "justify",
            fontSize: "3.6vw"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          dangerouslySetInnerHTML: {
            __html: dataOneBlog.desc
          }
        })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__["default"], null));
      }
    }
  }]);

  return BlogDesc;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (BlogDesc);

/***/ }),

/***/ "./resources/js/pages/Home.js":
/*!************************************!*\
  !*** ./resources/js/pages/Home.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Footer */ "./resources/js/components/Footer.js");
/* harmony import */ var _components_FooterMobile__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/FooterMobile */ "./resources/js/components/FooterMobile.js");
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/Navbar */ "./resources/js/components/Navbar.js");
/* harmony import */ var _components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/NavbarMobile */ "./resources/js/components/NavbarMobile.js");
/* harmony import */ var _components_Beranda_Section1__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/Beranda/Section1 */ "./resources/js/components/Beranda/Section1.js");
/* harmony import */ var _components_Beranda_Section2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/Beranda/Section2 */ "./resources/js/components/Beranda/Section2.js");
/* harmony import */ var _components_Beranda_Section3__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/Beranda/Section3 */ "./resources/js/components/Beranda/Section3.js");
/* harmony import */ var _components_Beranda_Section4__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/Beranda/Section4 */ "./resources/js/components/Beranda/Section4.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }














var Search = antd__WEBPACK_IMPORTED_MODULE_1__["Input"].Search;
var Home = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_12__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_12__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(Home, _Component);

  function Home() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Home);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Home)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth,
      searchText: ""
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_10___default.a.init({
        duration: 1000,
        delay: 50
      });

      _this.props.newsStore.homeproduct();
    };

    _this.handleWindowSizeChange = function () {
      _this.setState({
        width: window.innerWidth
      });
    };

    _this.handleSearch = function (e) {
      e.preventDefault();
      var data = {
        q: _this.state.searchText
      };

      _this.props.newsStore.search(data);

      _this.props.history.push("/search");
    };

    return _this;
  }

  _createClass(Home, [{
    key: "UNSAFE_componentWillMount",
    value: function UNSAFE_componentWillMount() {
      window.addEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var width = this.state.width;
      var isMobile = width <= 500;
      var dataProduct = this.props.newsStore.dataProduct; //untuk pc//

      if (!isMobile) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Navbar__WEBPACK_IMPORTED_MODULE_4__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Beranda_Section1__WEBPACK_IMPORTED_MODULE_6__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Beranda_Section2__WEBPACK_IMPORTED_MODULE_7__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Beranda_Section3__WEBPACK_IMPORTED_MODULE_8__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Beranda_Section4__WEBPACK_IMPORTED_MODULE_9__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Footer__WEBPACK_IMPORTED_MODULE_2__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["BackTop"], {
          visibilityHeight: 450
        }));
      } else {
        //untuk mobile//
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "section1-mobile",
          type: "flex",
          justify: "center",
          align: "middle"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
          onSubmit: this.handleSearch
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Search, {
          placeholder: "Cari Produk",
          style: {
            width: "100%",
            height: "6vh",
            padding: "0.7rem 1rem"
          },
          onChange: function onChange(e) {
            return _this2.setState({
              searchText: e.target.value
            });
          }
        })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          align: "middle",
          style: {
            padding: "2rem 1rem 2.5rem 1rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 6,
          align: "center"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/start-mobile.svg")
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3vw"
          }
        }, "Kualitas Terbaik")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 6,
          align: "center"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/guarentee-mobile.svg")
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3vw"
          }
        }, "Bergaransi")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 6,
          align: "center"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/cheaper-mobile.svg")
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3vw"
          }
        }, "Harga Terjangkau")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 6,
          align: "center"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/free-ong-mobile.svg")
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3vw"
          }
        }, "Gratis Pengiriman"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          style: {
            backgroundColor: "#f5f5f5",
            paddingTop: "2rem",
            paddingBottom: "3.5rem",
            paddingRight: "1.5rem",
            paddingLeft: "1.5rem"
          },
          align: "middle"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          align: "center",
          justify: "center"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/logo-mobile.svg")
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          align: "left"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3.5vw",
            color: "black",
            textAlign: "justify"
          }
        }, "Berdiri sejak tahun 2016 sebagai Startup yang fokus pada Periklanan Transportasi, Sticar Group\xA0berekspansi ke bisnis periklanan dan percetakan luar ruang (Outdoor Advertising) yang lebih luas dan mendirikan Printugo.", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), "Kami menyediakan platform jasa percetakan luar ruang untuk bisnis anda dengan kualitas baik dan harga terjangkau"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "section2-mobile",
          justify: "flex-start"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontWeight: "bold",
            fontSize: "4.5vw",
            color: "white"
          }
        }, "Proses Pemesanan"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          "data-aos": "slide-right",
          "data-aos-delay": "550",
          xs: 24,
          align: "left",
          style: {
            paddingTop: "1rem",
            paddingBottom: "1rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.pathname, "image/timeline-mobile.png")
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "section3-mobile",
          justify: "center"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          style: {
            paddingBottom: "1.5rem"
          },
          "data-aos": "slide-right",
          "data-aos-delay": "550",
          xs: 24,
          align: "center"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "4.5vw",
            fontWeight: "bold",
            paddingBottom: "1rem"
          }
        }, "Our Product"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Divider"], null)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          type: "flex",
          gutter: 30,
          justify: "space-around",
          "data-aos": "slide-right"
        }, dataProduct.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            xs: 12,
            align: "center",
            style: {
              paddingBottom: "1.5rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_11__["Link"], {
            to: "/productdetail/".concat(item.id)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Card"], {
            cover: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
              src: "".concat(window.location.origin, "/storage/").concat(item.image),
              style: {
                width: "100%",
                height: "auto"
              }
            })
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            style: {
              padding: "1rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "3.5vw"
            }
          }, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "3.5vw",
              fontWeight: "bold",
              color: "#2F80ED"
            }
          }, item.harga)))));
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "section4-mobile",
          gutter: 20
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          "data-aos": "slide-right",
          "data-aos-delay": "550",
          xs: {
            span: 24
          },
          align: "center",
          style: {
            paddingBottom: "1.5rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "4.5vw",
            fontWeight: "bold",
            paddingBottom: "1rem"
          }
        }, "Apa Kata Mereka"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Divider"], null)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: {
            span: 12,
            offset: 1
          },
          sm: {
            span: 12,
            offset: 1
          },
          className: "section4-left"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/foto-mas.png")
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: {
            span: 11
          },
          sm: {
            span: 11
          },
          className: "section4-right"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3.5vw"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          style: {
            color: "#2F80ED"
          }
        }, "Affan Awangga"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          style: {
            fontWeight: "bold"
          }
        }, "- OYO Rooms")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3vw",
            paddingTop: "1rem"
          }
        }, "\"Kalau soal kecepatan dan kualitas Sticar nomor satu. Hasilnya juga memuaskan. Kita pesan Signage untuk OYO di 100 titik dan selesai tempat waktu.\""))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_FooterMobile__WEBPACK_IMPORTED_MODULE_3__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["BackTop"], {
          visibilityHeight: 450
        }));
      }
    }
  }]);

  return Home;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ "./resources/js/pages/OurCustomer.js":
/*!*******************************************!*\
  !*** ./resources/js/pages/OurCustomer.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/Navbar */ "./resources/js/components/Navbar.js");
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/Footer */ "./resources/js/components/Footer.js");
/* harmony import */ var _components_FooterMobile__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/FooterMobile */ "./resources/js/components/FooterMobile.js");
/* harmony import */ var _components_NavbarMobile__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/NavbarMobile */ "./resources/js/components/NavbarMobile.js");
/* harmony import */ var _ant_design_icons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ant-design/icons */ "./node_modules/@ant-design/icons/es/index.js");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }











var OurCustomer = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_9__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_9__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(OurCustomer, _Component);

  function OurCustomer() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, OurCustomer);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(OurCustomer)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_3___default.a.init({
        duration: 1000,
        delay: 50
      });

      _this.props.newsStore.customer();
    };

    _this.handleWindowSizeChange = function () {
      _this.setState({
        width: window.innerWidth
      });
    };

    _this.handleClick = function (event) {
      _this.setState({
        currentPage: Number(event.target.id)
      });
    };

    return _this;
  }

  _createClass(OurCustomer, [{
    key: "UNSAFE_componentWillMount",
    value: function UNSAFE_componentWillMount() {
      window.addEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          width = _this$state.width,
          currentPage = _this$state.currentPage,
          todosPerPage = _this$state.todosPerPage;
      var isMobile = width <= 500;
      var dataCustomer = this.props.newsStore.dataCustomer;
      var currentTodos = dataCustomer;

      var handleListItemClick = function handleListItemClick(event, index) {
        if (index === 0) {
          _this2.props.newsStore.transport();
        }

        if (index === 1) {
          _this2.props.newsStore.marketing();
        }

        if (index === 2) {
          _this2.props.newsStore.office();
        }

        if (index === 3) {
          _this2.props.newsStore.signage();
        }

        if (index === 4) {
          _this2.props.newsStore.garmen();
        }

        if (index === 5) {
          _this2.props.newsStore.packagingCus();
        }

        _this2.setState({
          selectedIndex: index
        });
      };

      var menu = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "1",
        onClick: function onClick(event) {
          return handleListItemClick(event, 0);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Transport Advertising"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "2",
        onClick: function onClick(event) {
          return handleListItemClick(event, 1);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Marketing Prints"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "3",
        onClick: function onClick(event) {
          return handleListItemClick(event, 2);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Office Needs Prints"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "4",
        onClick: function onClick(event) {
          return handleListItemClick(event, 3);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Signage Advertising"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "5",
        onClick: function onClick(event) {
          return handleListItemClick(event, 4);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Garmen Prints"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "6",
        onClick: function onClick(event) {
          return handleListItemClick(event, 5);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Packaging Prints")); //untuk pc//

      if (!isMobile) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Navbar__WEBPACK_IMPORTED_MODULE_4__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "aboutsection1",
          type: "flex",
          align: "middle",
          justify: "center"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          md: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          "data-aos": "zoom-in-up",
          level: 1,
          style: {
            fontSize: "2.7vw",
            color: "white",
            textAlign: "center"
          }
        }, "Our Customer"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "productsection1"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 5
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "1.5vw",
            marginBottom: "1rem",
            fontWeight: "bold"
          }
        }, "All Category"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 0);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0,5rem"
          }
        }, "Transport Advertising"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 0);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0,5rem"
          }
        }, "Marketing Prints"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 0);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0,5rem"
          }
        }, "Office Needs Prints"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 0);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0,5rem"
          }
        }, "Signage Advertising"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 0);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0,5rem"
          }
        }, "Garmen Prints"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 0);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0,5rem"
          }
        }, "Packaging Prints"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 19
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], null, currentTodos.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            md: 8,
            style: {
              marginBottom: "2rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
            to: "/ourcustomerdesc"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Card"], {
            style: {
              width: "85%"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            src: "".concat(window.location.origin, "/storage/").concat(item.image),
            style: {
              width: "17.25vw",
              height: "22vh"
            }
          }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "product-list"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            md: 22
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "1vw",
              marginLeft: "0.7rem",
              fontWeight: "bold",
              color: "#2F80ED"
            }
          }, item.harga)))))));
        })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Footer__WEBPACK_IMPORTED_MODULE_5__["default"], null));
      } else {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_NavbarMobile__WEBPACK_IMPORTED_MODULE_7__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "aboutsection1-mobile",
          type: "flex",
          justify: "center"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 13
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "4.5vw",
            fontWeight: "bold",
            paddingBottom: "2rem"
          }
        }, "Our Customer")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 11
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Dropdown"], {
          overlay: menu
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Button"], {
          style: {
            width: "100%",
            textAlign: "left",
            height: "5vh"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          style: {
            paddingRight: "5rem"
          }
        }, "Transport Advertising"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["DownOutlined"], null))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "customersection1-mobile"
        }, currentTodos.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            xs: 24
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
            to: "/ourcustomerdesc"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Card"], {
            cover: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
              src: "".concat(window.location.origin, "/storage/").concat(item.image)
            })
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "product-list-mobile"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "4.5vw",
              fontWeight: "bold"
            }
          }, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            xs: 24,
            style: {
              paddingTop: "0.5rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "3vw",
              fontWeight: "bold"
            }
          }, item.harga)))))));
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_FooterMobile__WEBPACK_IMPORTED_MODULE_6__["default"], null));
      }
    }
  }]);

  return OurCustomer;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (OurCustomer);

/***/ }),

/***/ "./resources/js/pages/OurCustomerDesc.js":
/*!***********************************************!*\
  !*** ./resources/js/pages/OurCustomerDesc.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/Navbar */ "./resources/js/components/Navbar.js");
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/Footer */ "./resources/js/components/Footer.js");
/* harmony import */ var _components_FooterMobile__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/FooterMobile */ "./resources/js/components/FooterMobile.js");
/* harmony import */ var _components_NavbarMobile__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/NavbarMobile */ "./resources/js/components/NavbarMobile.js");
/* harmony import */ var _ant_design_icons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ant-design/icons */ "./node_modules/@ant-design/icons/es/index.js");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }











var OurCustomerDesc = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_9__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_9__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(OurCustomerDesc, _Component);

  function OurCustomerDesc() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, OurCustomerDesc);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(OurCustomerDesc)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth,
      currentPage: 1,
      todosPerPage: 3
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_3___default.a.init({
        duration: 1000,
        delay: 50
      });

      _this.props.newsStore.customer();
    };

    _this.handleWindowSizeChange = function () {
      _this.setState({
        width: window.innerWidth
      });
    };

    return _this;
  }

  _createClass(OurCustomerDesc, [{
    key: "UNSAFE_componentWillMount",
    value: function UNSAFE_componentWillMount() {
      window.addEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          width = _this$state.width,
          currentPage = _this$state.currentPage,
          todosPerPage = _this$state.todosPerPage;
      var isMobile = width <= 500;
      var dataCustomer = this.props.newsStore.dataCustomer;
      var indexOfLastTodo = currentPage * todosPerPage;
      var indexOfFirstTodo = indexOfLastTodo - todosPerPage;
      var currentTodos = dataCustomer.slice(indexOfFirstTodo, indexOfLastTodo);
      var pageNumbers = [];

      for (var i = 1; i <= Math.ceil(dataCustomer.length / todosPerPage); i++) {
        pageNumbers.push(i);
      }

      var renderPageNumbers = pageNumbers.map(function (number) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: number,
          id: number,
          onClick: _this2.handleClick
        }, number);
      }); //untuk pc//

      if (!isMobile) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Navbar__WEBPACK_IMPORTED_MODULE_4__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          style: {
            paddingTop: "9rem",
            paddingLeft: "7rem",
            paddingRight: "7rem",
            paddingBottom: "1rem",
            backgroundColor: "#f5f5f5"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 16,
          style: {
            paddingLeft: "8.5rem",
            paddingBottom: "1.5rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/grab1.png")
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 8
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/grab2.png")
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 8,
          style: {
            paddingLeft: "1.5rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/grab3.png")
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 16
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/grab4.png")
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          style: {
            paddingLeft: "12rem",
            paddingRight: "12rem",
            paddingTop: "1rem",
            paddingBottom: "9rem",
            backgroundColor: "#f5f5f5"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontWeight: "bold",
            fontSize: "1.3vw",
            paddingBottom: "0.8rem"
          }
        }, "Grab Advertising")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius quis in sagittis aliquet pellentesque eget ultrices tempor sed. Metus massa est quis volutpat, netus praesent tellus aliquet. Molestie enim mauris est etiam at risus. Massa proin at sodales dolor lectus nisl vel sit dictum. Fermentum sed enim nunc dui diam non. Neque, eu arcu auctor donec aenean mauris. Morbi sed tincidunt arcu, volutpat rhoncus blandit. Sed consequat enim eget gravida aliquam risus, sem. Montes, ante nisi quis urna sed neque, egestas hac. Aliquet ornare sagittis senectus amet, tellus sed est. Facilisi porta elit, condimentum porttitor lorem. Ut. \u2022 Maecenas amet tempor est \u2022 Maecenas amet tempor est \u2022 Maecenas amet tempor est Maecenas amet tempor est, aliquet netus gravida enim. Tortor cras enim egestas vulputate vitae et, elementum volutpat magna. Aliquam in arcu dictum sodales porta morbi suspendisse odio. Consequat egestas tincidunt vulputate hac aliquet nam purus euismod. Sit viverra convallis nulla eleifend netus viverra phasellus. Placerat consequat lorem nibh elit est. Dui, ornare nibh adipiscing duis nulla ullamcorper orci id."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Footer__WEBPACK_IMPORTED_MODULE_5__["default"], null));
      } else {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_NavbarMobile__WEBPACK_IMPORTED_MODULE_7__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "blogsection1-mobile"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          align: "left",
          style: {
            paddingBottom: "1rem",
            paddingTop: "1.5rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
          to: "/product"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/back.svg")
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/grab1.png"),
          style: {
            width: "100%",
            paddingBottom: "1rem"
          }
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          style: {
            paddingTop: "0.5rem",
            paddingBottom: "1rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "5vw",
            fontWeight: "bold"
          }
        }, "Grab Advertising"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3.7vw",
            paddintTop: "1rem"
          }
        }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sociis elit, neque, dignissim id. Id faucibus sapien purus donec cursus. Suspendisse et est justo neque, est volutpat vestibulum faucibus. Ornare nam sed velit feugiat aliquet. Sollicitudin condimentum egestas gravida semper odio interdum. Aliquet eu, neque imperdiet sed leo consectetur bibendum.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/grab2.png"),
          style: {
            width: "100%",
            paddingBottom: "1rem"
          }
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/grab3.png"),
          style: {
            width: "100%",
            paddingBottom: "1rem"
          }
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          style: {
            paddingBottom: "1rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3.7vw"
          }
        }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sociis elit, neque, dignissim id. Id faucibus sapien purus donec cursus. Suspendisse et est justo neque, est volutpat vestibulum faucibus. Ornare nam sed velit feugiat aliquet. Sollicitudin condimentum egestas gravida semper odio interdum. Aliquet eu, neque imperdiet sed leo consectetur bibendum.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/grab4.png"),
          style: {
            width: "100%",
            paddingBottom: "1rem"
          }
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_FooterMobile__WEBPACK_IMPORTED_MODULE_6__["default"], null));
      }
    }
  }]);

  return OurCustomerDesc;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (OurCustomerDesc);

/***/ }),

/***/ "./resources/js/pages/Product.js":
/*!***************************************!*\
  !*** ./resources/js/pages/Product.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Footer */ "./resources/js/components/Footer.js");
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Navbar */ "./resources/js/components/Navbar.js");
/* harmony import */ var _components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/FooterMobile */ "./resources/js/components/FooterMobile.js");
/* harmony import */ var _components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/NavbarMobile */ "./resources/js/components/NavbarMobile.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }










var Option = antd__WEBPACK_IMPORTED_MODULE_1__["Select"].Option;
var Product = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_8__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_8__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(Product, _Component);

  function Product() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Product);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Product)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth,
      currentPage: 1,
      activeMenu: false
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_7___default.a.init({
        duration: 1000,
        delay: 50
      });

      _this.props.newsStore.product();
    };

    _this.handleWindowSizeChange = function () {
      _this.setState({
        width: window.innerWidth
      });
    };

    _this.handleClick = function (event) {
      _this.setState({
        currentPage: Number(event.target.id)
      });
    };

    return _this;
  }

  _createClass(Product, [{
    key: "UNSAFE_componentWillMount",
    value: function UNSAFE_componentWillMount() {
      window.addEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "menuActive",
    value: function menuActive() {
      this.setState({
        activeMenu: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var width = this.state.width;
      var isMobile = width <= 500;
      var dataProduct = this.props.newsStore.dataProduct;
      var currentTodos = dataProduct;

      var handleListItemClick = function handleListItemClick(event, index) {
        if (index === 0) {
          _this2.props.newsStore.foto();
        }

        if (index === 1) {
          _this2.props.newsStore.promosi();
        }

        if (index === 2) {
          _this2.props.newsStore.kantor();
        }

        if (index === 3) {
          _this2.props.newsStore.kartunama();
        }

        if (index === 4) {
          _this2.props.newsStore.packaging();
        }

        if (index === 5) {
          _this2.props.newsStore.merchandise();
        }

        _this2.setState({
          selectedIndex: index
        });
      };

      var handleChange = function handleChange(index) {
        if (index == 0) {
          _this2.props.newsStore.foto();
        }

        if (index == 1) {
          _this2.props.newsStore.promosi();
        }

        if (index == 2) {
          _this2.props.newsStore.kantor();
        }

        if (index == 3) {
          _this2.props.newsStore.kartunama();
        }

        if (index == 4) {
          _this2.props.newsStore.packaging();
        }

        if (index == 5) {
          _this2.props.newsStore.merchandise();
        }
      }; //untuk pc//


      if (!isMobile) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Navbar__WEBPACK_IMPORTED_MODULE_3__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "productsection1"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 5
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "1.5vw",
            marginBottom: "1rem",
            fontWeight: "bold"
          }
        }, "Our Product"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 0);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.5rem"
          }
        }, "Foto"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 1);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.5rem"
          }
        }, "Kebutuhan Promosi"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 2);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.5rem"
          }
        }, "Kebutuhan Kantor"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 3);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.5rem"
          }
        }, "Kartu Nama"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 4);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.5rem"
          }
        }, "Packaging"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
          className: "link-category",
          style: {
            paddingLeft: 0,
            marginBottom: 0
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          onClick: function onClick(event) {
            return handleListItemClick(event, 5);
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.5rem"
          }
        }, "Merchandise"))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 19
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], null, currentTodos.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            md: 8,
            style: {
              marginBottom: "2rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
            to: "/productdetail/".concat(item.id)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Card"], {
            style: {
              width: "85%"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            src: "".concat(window.location.origin, "/storage/").concat(item.image),
            style: {
              width: "100%",
              height: "auto"
            }
          }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "product-list"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            md: 22
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "1vw",
              marginLeft: "0.7rem",
              fontWeight: "bold",
              color: "#2F80ED"
            }
          }, item.harga)))))));
        })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Footer__WEBPACK_IMPORTED_MODULE_2__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["BackTop"], {
          visibilityHeight: 450
        }));
      } else {
        //untuk mobile//
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "productsection1-mobile"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 13
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "4.5vw",
            fontWeight: "bold",
            paddingBottom: "2rem"
          }
        }, "Our Product")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 11
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Select"], {
          defaultValue: "0",
          onChange: handleChange,
          style: {
            width: "100%"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Option, {
          value: "0"
        }, "Foto"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Option, {
          value: "1"
        }, "Kebutuhan Promosi"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Option, {
          value: "2"
        }, "Kebutuhan Kantor"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Option, {
          value: "3"
        }, "Kartu Nama"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Option, {
          value: "4"
        }, "Packaging"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Option, {
          value: "5"
        }, "Merchandise"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          type: "flex",
          gutter: 30,
          justify: "space-around",
          "data-aos": "slide-right"
        }, currentTodos.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            xs: 12,
            align: "center",
            style: {
              paddingBottom: "1.5rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
            to: "/productdetail/".concat(item.id)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Card"], {
            cover: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
              src: "".concat(window.location.origin, "/storage/").concat(item.image),
              style: {
                width: "100%",
                height: "auto"
              }
            })
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            style: {
              padding: "1rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "3.5vw"
            }
          }, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "3.5vw",
              fontWeight: "bold",
              color: "#2F80ED"
            }
          }, item.harga)))));
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["BackTop"], {
          visibilityHeight: 450
        }));
      }
    }
  }]);

  return Product;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (Product);

/***/ }),

/***/ "./resources/js/pages/ProductDetail.js":
/*!*********************************************!*\
  !*** ./resources/js/pages/ProductDetail.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Footer */ "./resources/js/components/Footer.js");
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Navbar */ "./resources/js/components/Navbar.js");
/* harmony import */ var _components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/FooterMobile */ "./resources/js/components/FooterMobile.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _components_NavbarMobile__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/NavbarMobile */ "./resources/js/components/NavbarMobile.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }










var Home = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_8__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_8__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(Home, _Component);

  function Home() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Home);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Home)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth
    };

    _this.componentDidMount = function () {
      var idUrl = window.location.hash;
      var data = {
        id: idUrl.replace("#/productdetail/", "")
      };
      aos__WEBPACK_IMPORTED_MODULE_7___default.a.init({
        duration: 1000,
        delay: 50
      });

      _this.props.newsStore.onearticle(data);
    };

    _this.componentDidUpdate = function () {
      var idUrl = window.location.hash;
      var data = {
        id: idUrl.replace("#/productdetail/", "")
      };

      _this.props.newsStore.onearticle(data);
    };

    _this.handleWindowSizeChange = function () {
      _this.setState({
        width: window.innerWidth
      });
    };

    return _this;
  }

  _createClass(Home, [{
    key: "UNSAFE_componentWillMount",
    value: function UNSAFE_componentWillMount() {
      window.addEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "render",
    value: function render() {
      var width = this.state.width;
      var isMobile = width <= 500;
      var dataOneProduct = this.props.newsStore.dataOneProduct; //untuk pc//

      if (!isMobile) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Navbar__WEBPACK_IMPORTED_MODULE_3__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          align: "middle",
          className: "productdetailsection"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 14
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/storage/").concat(dataOneProduct.image),
          style: {
            width: "100%",
            height: "auto"
          }
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 10,
          style: {
            paddingLeft: "2rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "1.5vw",
            fontWeight: "bold"
          }
        }, dataOneProduct.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Divider"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, dataOneProduct.harga), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, "Price includes GST and Shipping"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            paddingBottom: "2rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          dangerouslySetInnerHTML: {
            __html: dataOneProduct.desc
          }
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Button"], {
          type: "primary",
          block: true,
          style: {
            height: "6vh"
          },
          href: "https://wa.wizard.id/00bdac",
          target: "_blank",
          size: "large"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, "Order Now ")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Footer__WEBPACK_IMPORTED_MODULE_2__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["BackTop"], {
          visibilityHeight: 450
        }));
      } else {
        //untuk mobile//
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_NavbarMobile__WEBPACK_IMPORTED_MODULE_6__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "blogsection1-mobile"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          align: "left",
          style: {
            paddingBottom: "1rem",
            paddingTop: "1.5rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__["Link"], {
          to: "/product"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/image/back.svg")
        }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: "".concat(window.location.origin, "/storage/").concat(dataOneProduct.image),
          style: {
            width: "100%",
            paddingBottom: "1rem"
          }
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 24,
          style: {
            paddingTop: "0.5rem",
            paddingBottom: "2rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "5vw",
            fontWeight: "bold"
          }
        }, dataOneProduct.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Divider"], {
          style: {
            height: "2px",
            width: "100%",
            background: "#afafaf"
          }
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "4.8vw",
            fontWeight: "bold"
          }
        }, dataOneProduct.harga), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "3.9vw",
            fontWeight: "bold",
            paddingBottom: "1rem",
            color: "#afafaf"
          }
        }, "Price includes GST and Shipping"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            paddingBottom: "2rem"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          dangerouslySetInnerHTML: {
            __html: dataOneProduct.desc
          }
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Button"], {
          type: "primary",
          block: true,
          style: {
            height: "8vh"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
          style: {
            color: "white",
            fontWeight: "bold",
            fontSize: "4.5vw"
          },
          href: "https://wa.wizard.id/00bdac",
          target: "_blank"
        }, "Order Now")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["BackTop"], {
          visibilityHeight: 450
        }));
      }
    }
  }]);

  return Home;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ "./resources/js/pages/SearchResult.js":
/*!********************************************!*\
  !*** ./resources/js/pages/SearchResult.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _components_Footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Footer */ "./resources/js/components/Footer.js");
/* harmony import */ var _components_Navbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Navbar */ "./resources/js/components/Navbar.js");
/* harmony import */ var _components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/FooterMobile */ "./resources/js/components/FooterMobile.js");
/* harmony import */ var _components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/NavbarMobile */ "./resources/js/components/NavbarMobile.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! aos */ "./node_modules/aos/dist/aos.js");
/* harmony import */ var aos__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(aos__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _ant_design_icons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ant-design/icons */ "./node_modules/@ant-design/icons/es/index.js");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobxreact.esm.js");
var _dec, _class, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }











var SearchResult = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_9__["inject"])("newsStore"), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_9__["observer"])(_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(SearchResult, _Component);

  function SearchResult() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, SearchResult);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(SearchResult)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      width: window.innerWidth,
      currentPage: 1,
      todosPerPage: 3
    };

    _this.componentDidMount = function () {
      aos__WEBPACK_IMPORTED_MODULE_7___default.a.init({
        duration: 1000,
        delay: 50
      });
    };

    _this.handleWindowSizeChange = function () {
      _this.setState({
        width: window.innerWidth
      });
    };

    return _this;
  }

  _createClass(SearchResult, [{
    key: "UNSAFE_componentWillMount",
    value: function UNSAFE_componentWillMount() {
      window.addEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener("resize", this.handleWindowSizeChange);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          width = _this$state.width,
          currentPage = _this$state.currentPage,
          todosPerPage = _this$state.todosPerPage;
      var isMobile = width <= 500;
      var dataSearch = this.props.newsStore.dataSearch;
      var indexOfLastTodo = currentPage * todosPerPage;
      var indexOfFirstTodo = indexOfLastTodo - todosPerPage;
      var currentTodos = dataSearch.slice(indexOfFirstTodo, indexOfLastTodo);
      var pageNumbers = [];

      for (var i = 1; i <= Math.ceil(dataSearch.length / todosPerPage); i++) {
        pageNumbers.push(i);
      }

      var renderPageNumbers = pageNumbers.map(function (number) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
          key: number,
          id: number,
          onClick: _this2.handleClick
        }, number);
      });
      var menu = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "1",
        onClick: function onClick(event) {
          return handleListItemClick(event, 0);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Foto"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "2",
        onClick: function onClick(event) {
          return handleListItemClick(event, 1);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Kebutuhan Promosi"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "3",
        onClick: function onClick(event) {
          return handleListItemClick(event, 2);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Kebutuhan Kantor"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "4",
        onClick: function onClick(event) {
          return handleListItemClick(event, 3);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Kartu Nama"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "5",
        onClick: function onClick(event) {
          return handleListItemClick(event, 4);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Packaging"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: "6",
        onClick: function onClick(event) {
          return handleListItemClick(event, 5);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["UserOutlined"], null), "Merchandise")); //untuk pc//

      if (!isMobile) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Navbar__WEBPACK_IMPORTED_MODULE_3__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "productsection1"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 5
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "1.5vw",
            marginBottom: "1rem",
            fontWeight: "bold"
          }
        }, "Our product"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          to: ""
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.6rem"
          }
        }, "Foto")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          to: ""
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.6rem"
          }
        }, "Kebutuhan Promosi")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          to: ""
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.6rem"
          }
        }, "Kebutuhan Kantor")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          to: ""
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.6rem"
          }
        }, "Kartu Nama")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          to: ""
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.6rem"
          }
        }, "Packaging")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
          to: ""
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            marginBottom: "0.6rem"
          }
        }, "Merchandise"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          md: 19
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], null, currentTodos.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            md: 8
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
            to: "/productdetail"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Card"], {
            style: {
              width: "85%"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
            style: {
              width: "100%",
              height: "auto"
            },
            src: "".concat(window.location.origin, "/storage/").concat(item.image)
          }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            className: "product-list"
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], null, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            md: 22
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "1vw",
              marginLeft: "0.7rem",
              fontWeight: "bold",
              color: "#2F80ED"
            }
          }, item.harga)))))));
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          style: {
            marginTop: 50
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          id: "page-numbers"
        }, renderPageNumbers))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Footer__WEBPACK_IMPORTED_MODULE_2__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["BackTop"], {
          visibilityHeight: 450
        }));
      } else {
        //untuk mobile//
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_NavbarMobile__WEBPACK_IMPORTED_MODULE_5__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          className: "productsection1-mobile"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 13
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
          style: {
            fontSize: "4.5vw",
            fontWeight: "bold",
            paddingBottom: "2rem"
          }
        }, "Our Product")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
          xs: 11
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Dropdown"], {
          overlay: menu
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Button"], {
          style: {
            width: "100%",
            textAlign: "left"
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          style: {
            paddingRight: "5rem"
          }
        }, "Foto"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_8__["DownOutlined"], null)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          type: "flex",
          gutter: 30,
          justify: "space-around",
          "data-aos": "slide-right"
        }, currentTodos.map(function (item) {
          return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], {
            xs: 12,
            align: "center",
            style: {
              paddingBottom: "1.5rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["Link"], {
            to: "/productdetail/".concat(item.id)
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Card"], {
            cover: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
              src: "".concat(window.location.origin, "/storage/").concat(item.image),
              style: {
                width: "100%",
                height: "auto"
              }
            })
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
            style: {
              padding: "1rem"
            }
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "3.5vw"
            }
          }, item.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Typography"], {
            style: {
              fontSize: "3.5vw",
              fontWeight: "bold",
              color: "#2F80ED"
            }
          }, item.harga)))));
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Row"], {
          style: {
            marginTop: 50
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
          id: "page-numbers"
        }, renderPageNumbers)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_FooterMobile__WEBPACK_IMPORTED_MODULE_4__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["BackTop"], {
          visibilityHeight: 450
        }));
      }
    }
  }]);

  return SearchResult;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (SearchResult);

/***/ }),

/***/ "./resources/js/store/newsStore.js":
/*!*****************************************!*\
  !*** ./resources/js/store/newsStore.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! mobx */ "./node_modules/mobx/lib/mobx.module.js");
var _class, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _temp;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object.keys(descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object.defineProperty(target, property, desc); desc = null; } return desc; }

function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and runs after the decorators transform.'); }



var NewsStore = (_class = (_temp = /*#__PURE__*/function (_Component) {
  _inherits(NewsStore, _Component);

  function NewsStore() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, NewsStore);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(NewsStore)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _initializerDefineProperty(_this, "dataNews", _descriptor, _assertThisInitialized(_this));

    _initializerDefineProperty(_this, "dataCustomer", _descriptor2, _assertThisInitialized(_this));

    _initializerDefineProperty(_this, "dataProduct", _descriptor3, _assertThisInitialized(_this));

    _initializerDefineProperty(_this, "dataOneProduct", _descriptor4, _assertThisInitialized(_this));

    _initializerDefineProperty(_this, "dataOneBlog", _descriptor5, _assertThisInitialized(_this));

    _initializerDefineProperty(_this, "dataHome", _descriptor6, _assertThisInitialized(_this));

    _initializerDefineProperty(_this, "dataSearch", _descriptor7, _assertThisInitialized(_this));

    return _this;
  }

  _createClass(NewsStore, [{
    key: "news",
    value: function news() {
      var _this2 = this;

      fetch("api/blog", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this2.dataNews = res.message;
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "customer",
    value: function customer() {
      var _this3 = this;

      fetch("api/customer", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this3.dataCustomer = res.message.filter(function (item) {
              return item.kategori === "transport";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "product",
    value: function product() {
      var _this4 = this;

      fetch("api/product", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this4.dataProduct = res.message.filter(function (item) {
              return item.kategori === "foto";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "allproduct",
    value: function allproduct() {
      var _this5 = this;

      fetch("api/product", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this5.dataProduct = res.message;
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "homeproduct",
    value: function homeproduct() {
      var _this6 = this;

      fetch("api/homeproduct", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this6.dataProduct = res.message;
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "home",
    value: function home() {
      var _this7 = this;

      fetch("api/home", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this7.dataHome = res.message;
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "search",
    value: function search(data) {
      var _this8 = this;

      fetch("api/search", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this8.dataSearch = res.message;
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "promosi",
    value: function promosi() {
      var _this9 = this;

      fetch("api/product", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this9.dataProduct = res.message.filter(function (item) {
              return item.kategori === "promosi";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "foto",
    value: function foto() {
      var _this10 = this;

      fetch("api/product", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this10.dataProduct = res.message.filter(function (item) {
              return item.kategori === "foto";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "kantor",
    value: function kantor() {
      var _this11 = this;

      fetch("api/product", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this11.dataProduct = res.message.filter(function (item) {
              return item.kategori === "kantor";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "kartunama",
    value: function kartunama() {
      var _this12 = this;

      fetch("api/product", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this12.dataProduct = res.message.filter(function (item) {
              return item.kategori === "kartu";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "packaging",
    value: function packaging() {
      var _this13 = this;

      fetch("api/product", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this13.dataProduct = res.message.filter(function (item) {
              return item.kategori === "pack";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "merchandise",
    value: function merchandise() {
      var _this14 = this;

      fetch("api/product", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this14.dataProduct = res.message.filter(function (item) {
              return item.kategori === "merc";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "onearticle",
    value: function onearticle(data) {
      var _this15 = this;

      fetch("api/oneproduct", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this15.dataOneProduct = res.message[0];
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "oneblog",
    value: function oneblog(data) {
      var _this16 = this;

      fetch("api/oneblog", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this16.dataOneBlog = res.message[0];
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "transport",
    value: function transport() {
      var _this17 = this;

      fetch("api/customer", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this17.dataCustomer = res.message.filter(function (item) {
              return item.kategori === "transport";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "marketing",
    value: function marketing() {
      var _this18 = this;

      fetch("api/customer", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this18.dataCustomer = res.message.filter(function (item) {
              return item.kategori === "marketing";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "office",
    value: function office() {
      var _this19 = this;

      fetch("api/customer", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this19.dataCustomer = res.message.filter(function (item) {
              return item.kategori === "office";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "signage",
    value: function signage() {
      var _this20 = this;

      fetch("api/customer", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this20.dataCustomer = res.message.filter(function (item) {
              return item.kategori === "signage";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "garmen",
    value: function garmen() {
      var _this21 = this;

      fetch("api/customer", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this21.dataCustomer = res.message.filter(function (item) {
              return item.kategori === "garmen";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }, {
    key: "packagingCus",
    value: function packagingCus() {
      var _this22 = this;

      fetch("api/customer", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function (res) {
        return res.json();
      }).then(function (res) {
        if (res.success) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_1__["runInAction"])(function () {
            _this22.dataCustomer = res.message.filter(function (item) {
              return item.kategori === "packaging";
            });
          });
        } else {
          console.log("error");
        }
      })["catch"](function (err) {
        return console.log(err);
      });
    }
  }]);

  return NewsStore;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]), _temp), (_descriptor = _applyDecoratedDescriptor(_class.prototype, "dataNews", [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], {
  configurable: true,
  enumerable: true,
  writable: true,
  initializer: function initializer() {
    return [];
  }
}), _descriptor2 = _applyDecoratedDescriptor(_class.prototype, "dataCustomer", [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], {
  configurable: true,
  enumerable: true,
  writable: true,
  initializer: function initializer() {
    return [];
  }
}), _descriptor3 = _applyDecoratedDescriptor(_class.prototype, "dataProduct", [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], {
  configurable: true,
  enumerable: true,
  writable: true,
  initializer: function initializer() {
    return [];
  }
}), _descriptor4 = _applyDecoratedDescriptor(_class.prototype, "dataOneProduct", [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], {
  configurable: true,
  enumerable: true,
  writable: true,
  initializer: function initializer() {
    return [];
  }
}), _descriptor5 = _applyDecoratedDescriptor(_class.prototype, "dataOneBlog", [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], {
  configurable: true,
  enumerable: true,
  writable: true,
  initializer: function initializer() {
    return [];
  }
}), _descriptor6 = _applyDecoratedDescriptor(_class.prototype, "dataHome", [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], {
  configurable: true,
  enumerable: true,
  writable: true,
  initializer: function initializer() {
    return [];
  }
}), _descriptor7 = _applyDecoratedDescriptor(_class.prototype, "dataSearch", [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], {
  configurable: true,
  enumerable: true,
  writable: true,
  initializer: function initializer() {
    return [];
  }
}), _applyDecoratedDescriptor(_class.prototype, "news", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "news"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "customer", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "customer"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "product", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "product"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "allproduct", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "allproduct"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "homeproduct", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "homeproduct"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "home", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "home"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "search", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "search"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "promosi", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "promosi"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "foto", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "foto"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "kantor", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "kantor"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "kartunama", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "kartunama"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "packaging", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "packaging"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "merchandise", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "merchandise"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "onearticle", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "onearticle"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "oneblog", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "oneblog"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "transport", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "transport"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "marketing", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "marketing"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "office", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "office"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "signage", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "signage"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "garmen", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "garmen"), _class.prototype), _applyDecoratedDescriptor(_class.prototype, "packagingCus", [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, "packagingCus"), _class.prototype)), _class);
/* harmony default export */ __webpack_exports__["default"] = (new NewsStore());

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/app.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\Users\farhatdk\Documents\printugo.com\resources\js\app.js */"./resources/js/app.js");
module.exports = __webpack_require__(/*! C:\Users\farhatdk\Documents\printugo.com\resources\sass\app.scss */"./resources/sass/app.scss");


/***/ })

},[[0,"/js/manifest","/js/vendor"]]]);